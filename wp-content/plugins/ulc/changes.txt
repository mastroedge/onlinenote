Version 1.1.8 ( 22th October 2017 )
  - pagination pages dsplay likes icon
  - added random order by

Version 1.1.7 ( 19th October 2017 )
  - fix first plugin initialization

Version 1.1.6 ( 28th August 2017 )
  - fix links order by query

Version 1.1.5 ( 8th August 2017 )
  - fix for style 'font-awensome'

Version 1.1.4 ( 8th August 2017 )
  - added counter for likes.

Version 1.1.3 ( 20th July 2017 )
  - fix CMB2 Condition bug and Visual Composer.

Version 1.1.2 ( 20th January 2017 )
  - fix shortcode warning.
  - fix unexpected output.

Version 1.1.1 ( 21th December 2016 )
  - css styles.
  - clean up js.

Version 1.1.0 ( 11th December 2016 )
  - Added clicks and likes.
  - Statistic page and Reset button.
  - Added favion for items.
  - Added search bar(works only with non paging list).
  - Cleaned up and updated styles.
  - Fix bug with filter and search.

Version 1.0.2 ( 27th November 2016 )
  - Fix tooltip on change pagination.

Version 1.0.1 ( 25th November 2016 )
  - Fix mobile devises popup position.
  - Fix masonry jump if change smaller list then current.

Version 1.0.0 ( 23th November 2016 )
  - Initial public release.