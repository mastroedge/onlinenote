<?php
if ( is_admin() ) {

  function remove_revolution_slider_meta_boxes() {
    remove_meta_box( 'mymetabox_revslider_0', 'ulc-link', 'normal' );
    remove_meta_box( 'mymetabox_revslider_0', 'ulc-shortcode', 'normal' );
  }

  add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );

}
?>