<?php
function ulc_stats_panel(){
  add_submenu_page('edit.php?post_type=ulc-link', 'ULC Statistic', 'ULC Statistic', 'manage_options', 'ulc-links-stats', 'ulc_stats_panel_funct');
}

add_action('admin_menu', 'ulc_stats_panel');
function ulc_stats_panel_funct(){

    wp_register_script( 'list.js', ULC_PLUGIN_URL . 'assets/admin/js/plugins/list.min.js', array(), ULC_VERSION, true );
    wp_register_script( 'chart.js', ULC_PLUGIN_URL . 'assets/admin/js/plugins/Chart.min.js', array(), ULC_VERSION, true );
    wp_register_script( 'ulc-admin', ULC_PLUGIN_URL . 'assets/admin/js/main.js', array('jquery', 'chart.js', 'list.js'), ULC_VERSION, false );

    wp_register_style( 'ulc_main', ULC_PLUGIN_URL . 'assets/admin/css/style.css', array(), ULC_VERSION, 'all' );
    wp_enqueue_style( 'ulc_main' );


    $ulc_admin_obj = array(
        'ajaxurl' => admin_url("admin-ajax.php"),
        'homeUrl' => esc_url( home_url( '/' ) ),
        'token' => wp_create_nonce( "ulc-ajax-admin-token" ),
        'pluginUrl' => esc_html( ULC_PLUGIN_URL )
    );
    wp_localize_script('ulc-admin', 'ulcAdminObj', $ulc_admin_obj);

    wp_enqueue_script('ulc-admin');
    wp_enqueue_script('chart.js');

    $prefix_link = '_ulc_link';
    echo '<div class="wrap">';

        $links_loop_args = array(
            'post_type' => 'ulc-link',
            'post_status' => 'publish',
            'posts_per_page' => 25,
            'order' => 'DESC',
            'meta_key' => $prefix_link . '_stats_clicks',
            'orderby'  => 'meta_value_num',
        ); ?>
        <h2>ULC Top 25 links</h2>
        <canvas id="ulc-click-chart"></canvas>
        <?php

        $links_loop = new WP_Query($links_loop_args);

        if ($links_loop) {
            while ($links_loop->have_posts()) {
                $links_loop->the_post();

                $labels[] = get_the_title();

                $click = intval(get_post_meta(get_the_ID(), $prefix_link . '_stats_clicks', true));
                $clicks[] = $click;

                $like = intval(get_post_meta(get_the_ID(), $prefix_link . '_stats_likes', true));
                $likes[] = $like;
            }

            if(!empty($labels)) {
                echo '<script> var ulc_stat_data_labels ='.json_encode($labels).', ulc_stat_data_clicks='.json_encode($clicks).', ulc_stat_data_likes='.json_encode($likes).';</script>';
            }
            else {
                echo '<p>No Statistic yet.</p>';
            }
        }

        wp_reset_query(); ?>

        <!-- Sort table -->
        <?php
        $links_loop_table_args = array(
            'post_type' => 'ulc-link',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'DESC',
            'meta_key' => $prefix_link . '_stats_likes',
            'orderby'  => 'meta_value_num',

        );

        $links_loop_table = new WP_Query($links_loop_table_args);

        if ($links_loop_table && !empty($labels)):
            $prefix_link = '_ulc_link'; $number = 0; ?>

        <h2>ULC links statistic</h2>
        <div id="ulc-table-links" class="ulc-table-links">
        <input class="search" placeholder="Search" type="search"/>
          <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <td width="30px" class="manage-column column-title column-primary sortable desc">
                      <a class="sort asc" data-sort="number">#</a>
                    </td>
                    <td width="70%" class="manage-column column-title column-primary sortable desc">
                      <a class="sort desc" data-sort="title">
                        Title
                      </a>
                    </td>
                    <td class="manage-column column-title column-primary text-center sortable desc">
                      <a class="sort desc" data-sort="clicks">
                        Clicks
                      </a>
                    </td>
                    <td class="manage-column column-title column-primary text-center sortable desc">
                      <a class="sort desc" data-sort="likes">
                        Likes
                      </a>
                    </td>
                </tr>
            </thead>
            <!-- IMPORTANT, class="list" have to be at tbody -->
            <tbody class="list">
            <?php
            while ($links_loop_table->have_posts()) {
                $links_loop_table->the_post(); $number++; ?>
                <tr>
                    <td class="number"><?php echo $number; ?></td>
                    <td class="title"><?php echo get_the_title(); ?></td>
                    <?php $click = intval(get_post_meta(get_the_ID(), $prefix_link . '_stats_clicks', true)); ?>
                    <td class="clicks text-center"><?php echo $click; ?></td>
                    <?php $like = intval(get_post_meta(get_the_ID(), $prefix_link . '_stats_likes', true)); ?>
                    <td class="likes text-center"><?php echo $like; ?></td>
                </tr>
                <?php
            }

        ?>
            </tbody>
          </table>
        </div>
        <?php
        echo '<p><button id="js-reset-stat" class="button button-primary button-large">Reset Statistic</button></>';
        echo '</div>';
        ?>
        <?php endif; ?>

        <?php wp_reset_query(); ?>
        <!-- End Sort table -->
        <?php
}


// Reset all statistic
add_action('wp_ajax_ulc_links_stat_reset', 'ulc_ajax_links_stat_reset');
add_action('wp_ajax_nopriv_ulc_links_stat_reset', 'ulc_ajax_links_stat_reset');
function ulc_ajax_links_stat_reset() {
    check_ajax_referer('ulc-ajax-admin-token', 'token');
    $links_loop_args = array(
        'post_type' => 'ulc-link',
        'posts_per_page' => -1,
    );
    $ret = false;
    $links_loop = new WP_Query($links_loop_args);
    if ($links_loop) {
        $prefix_link = '_ulc_link';
        while ($links_loop->have_posts()) {
            $links_loop->the_post();
            update_post_meta( get_the_ID(), $prefix_link . '_stats_clicks', 0 );
            update_post_meta( get_the_ID(), $prefix_link . '_stats_likes', 0 );
        }

        $ret = 'success';
        return $ret;
    }
    wp_reset_postdata();

    wp_send_json_success($ret);
    die();
}
?>