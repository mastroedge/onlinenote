<?php
add_action('cmb2_admin_init', 'ulc_metaboxes_shortcode');

function ulc_metaboxes_shortcode()
{

    $prefix_link = '_ulc_shortcode';

    $cmb_general = new_cmb2_box(array(
        'id' => 'ulc_shortcode_general',
        'title' => __('Shortcode general', 'cmb2'),
        'object_types' => array('ulc-shortcode'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));

    $cmb_general->add_field(array(
        'name' => 'Category to display',
        'desc' => '',
        'id' => $prefix_link . 'taxonomy_select',
        'type' => 'multicheck',
        'options_cb' => 'ulc_get_link_type_list'
    ));

    $cmb_general->add_field(array(
        'name' => __('Enable filter', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'is_filter',
        'default' => false,
        'type' => 'checkbox',
    ));

    $cmb_general->add_field(array(
        'name' => __('Enable searh bar', 'cmb2'),
        'desc' => __('If post "Post per page" value equal -1, search bar will display', 'cmb2'),
        'id' => $prefix_link . 'is_search_bar',
        'default' => false,
        'type' => 'checkbox',
    ));

    $cmb_general->add_field(array(
        'name' => __('Show likes', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'is_likes',
        'default' => false,
        'type' => 'checkbox',
    ));

    $cmb_general->add_field(array(
        'name' => __('Show likes counter', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'is_likes_count',
        'default' => false,
        'type' => 'checkbox',
        'attributes' => array(
            'data-conditional-id'    => $prefix_link . 'is_likes',
            'data-conditional-value' => 'on'
        )
    ));

    $cmb_general->add_field(array(
        'name' => __('Layout style', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'layout_style',
        'type' => 'select',
        'default' => 'title',
        'options' => array(
            'default' => __('Default', 'cmb2'),
            'inverse' => __('Inverse', 'cmb2'),
            'flat' => __('Flat', 'cmb2'),
            'minimal' => __('Minimal', 'cmb2'),
        ),
    ));

    $cmb_general->add_field(array(
        'name' => __('Columns per row', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'cols',
        'type' => 'select',
        'default' => '1',
        'options' => array(
            '1' => __('One column', 'cmb2'),
            '2' => __('Two columns', 'cmb2'),
            '3' => __('Three columns', 'cmb2'),
            '4' => __('Four columns', 'cmb2'),
            '5' => __('Five columns', 'cmb2'),
            '6' => __('Six columns', 'cmb2'),
            '7' => __('Seven columns', 'cmb2'),
            '8' => __('Eight columns', 'cmb2'),
        ),
    ));

    $cmb_general->add_field(array(
        'name' => __('Post per page', 'cmb2'),
        'desc' => __('Use "-1" to show all posts or number > than 1', 'cmb2'),
        'id' => $prefix_link . 'posts_per_page',
        'default' => '-1',
        'type' => 'text',
    ));

    $cmb_general->add_field(array(
        'name' => __('Order by', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'order_by',
        'type' => 'select',
        'default' => 'title',
        'options' => array(
            'clicks' => __('Clicks', 'cmb2'),
            'likes' => __('Likes', 'cmb2'),
            'ID' => __('ID', 'cmb2'),
            'author' => __('Author', 'cmb2'),
            'title' => __('Title', 'cmb2'),
            'type' => __('Type', 'cmb2'),
            'date' => __('Date', 'cmb2'),
            'rand' => __('Random', 'cmb2' ),
            'menu_order' => __('Menu order', 'cmb2'),
        ),
    ));


    $cmb_general->add_field(array(
        'name' => __('Order', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix_link . 'order',
        'type' => 'select',
        'default' => 'ASC',
        'options' => array(
            'ASC' => __('ASC', 'cmb2'),
            'DESC' => __('DESC', 'cmb2'),
        ),
    ));

}

function ulc_get_link_type_list()
{
    $taxonomy_list = array();

    $taxonomy_query = get_terms(array(
        'taxonomy' => 'ulc-link-type',
        'hide_empty' => false
    ));

    if(!empty($taxonomy_query)) {
        foreach ($taxonomy_query as $taxonomy_term) {
            $taxonomy_list[$taxonomy_term->slug] = $taxonomy_term->name;
        }
    } else {
        $taxonomy_list = false;
    }
    wp_reset_postdata();
    return $taxonomy_list;
}

?>