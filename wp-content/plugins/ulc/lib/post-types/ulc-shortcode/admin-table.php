<?php
add_filter('manage_ulc-shortcode_posts_columns', 'ulc_shortcode_table_head');
function ulc_shortcode_table_head( $defaults ) {
  $defaults['shortcode']  = 'Shortcode';
  return $defaults;
}


add_action( 'manage_ulc-shortcode_posts_custom_column', 'ulc_shortcode_table_content', 10, 2 );

function ulc_shortcode_table_content( $column_name, $post_id ) {
    if ($column_name == 'shortcode') {
    $attrs = '';
    $prefix_link = '_ulc_shortcode';

    // Taxonomies
    $tax_list_meta = get_post_meta($post_id, $prefix_link . 'taxonomy_select', true);
    if(intval(wp_count_terms('ulc-link-type')) === count($tax_list_meta)) {
      $taxonomy_select = '*';
    } else {
      $tax_list_meta_id = array();
      foreach ($tax_list_meta as $taxonomy_slug) {
        $term = get_term_by('slug', $taxonomy_slug, 'ulc-link-type');
        if(!empty($term)) {
          $tax_list_meta_id[] = $term->term_id;
        }
      }
      $taxonomy_select = implode(';', $tax_list_meta_id);
    }

    $layout_style = get_post_meta($post_id, $prefix_link . 'layout_style', true);
    $cols = get_post_meta($post_id, $prefix_link . 'cols', true);

    $posts_per_page = get_post_meta($post_id, $prefix_link . 'posts_per_page', true);
    $order_by = get_post_meta($post_id, $prefix_link . 'order_by', true);
    $order = get_post_meta($post_id, $prefix_link . 'order', true);
    $is_search_bar = get_post_meta($post_id, $prefix_link . 'is_search_bar', true);
    $is_likes = get_post_meta($post_id, $prefix_link . 'is_likes', true);
    $is_likes_count = get_post_meta($post_id, $prefix_link . 'is_likes_count', true);

    $attrs .= ' taxs="' . $taxonomy_select . '"';
    $attrs .= ' posts_per_page="' . $posts_per_page . '"';
    $attrs .= ' order_by="' . $order_by . '"';
    $attrs .= ' order="' . $order . '"';
    $attrs .= ' cols="' . $cols . '"';

    if(!empty($is_likes)) {
      $attrs .= ' is_likes="' . $is_likes . '"';
    }

    if(!empty($is_likes) && !empty($is_likes_count)) {
      $attrs .= ' is_likes_count="' . $is_likes_count . '"';
    }    

    $is_filter = get_post_meta($post_id, $prefix_link . 'is_filter', true);
    if(!empty($is_filter)) {
      $attrs .= ' is_filter="' . $is_filter . '"';
    }
    if(!empty($is_search_bar) && intval($posts_per_page) === -1) {
      $attrs .= ' is_search_bar="' . $is_search_bar . '"';
    }

    $attrs .= ' layout_style="' . $layout_style . '"';

    $output = '';

    $shortcode = htmlentities('[ulc id="' . $post_id . '"' . $attrs . ']');
    $output .= '<input type="text" class="widefat urlfield" readonly="readonly" value="'.$shortcode.'" />';
    echo $output;
  }
}

?>