<?php

// Post type
add_action('init', 'ulc_register_post_type_ulc_shortcode', 0);

function ulc_register_post_type_ulc_shortcode() {

  register_post_type('ulc-shortcode', array(
      'labels' => array(
          'name' => __('ULC Shortcode', 'ulc'),
          'singular_name' => __('ULC Shortcode', 'ulc'),
          'add_new' => __('Add', 'ulc'),
          'add_new_item' => __('Add ULC Shortcode', 'ulc'),
          'edit_item' => __('Edit ULC Shortcode', 'ulc'),
          'new_item' => __('New ULC Shortcode', 'ulc'),
          'view_item' => __('View ULC Shortcode', 'ulc'),
          'search_items' => __('Search ULC shortcode', 'ulc'),
          'not_found' => __('No ULC shortcode found', 'ulc'),
          'not_found_in_trash' => __('No ULC shortcode found in trash', 'ulc'),
          'menu_icon'  => 'dashicons-admin-links'

      ),
      'public' => false,
      'show_ui' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => false,
      'query_var' => false,
      'supports' => array('title', 'author'),
      'show_in_menu' => 'edit.php?post_type=ulc-link',
  ));

}

?>