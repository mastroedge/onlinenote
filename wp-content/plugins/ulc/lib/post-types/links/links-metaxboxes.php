<?php
add_action('cmb2_admin_init', 'ulc_metaboxes_links');

function ulc_metaboxes_links()
{

    $prefix = '_ulc_link';

    $cmb_image = new_cmb2_box(array(
        'id' => 'links_image',
        'title' => __('Link logo', 'cmb2'),
        'object_types' => array('ulc-link'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));

    //Image type
    $cmb_image->add_field( array(
        'name'             => 'Logo image type',
        'id'               => $prefix . '_img_type',
        'type'             => 'radio',
        'show_option_none' => true,
        // 'default'          => 'favicon',
        'options'          => array(
            'favicon'     => 'Favicon',
            'image'     => 'Upload image',
            'font_icon' => 'Font icon',
        ),
        'attributes'       => array(
            'required'       => 'required',
        ),
    ) );

        // Favicon
        $cmb_image->add_field( array(
            'name'       => 'Favicon path',
            'desc'       => 'If this field empty, plugin will try to find favicon automatically.',
            'id'         => $prefix . '_img_favicon_url',
            'type'       => 'text_url',
            'attributes' => array(
                'data-conditional-id'    => $prefix . '_img_type',
                'data-conditional-value' => 'favicon',
            ),
        ) );

        // Image
        $cmb_image->add_field( array(
            'name' => 'Link logo image',
            'desc' => 'Image size 80x80 px',
            'id' => $prefix . 'logo_img',
            'type'       => 'text',
            'type' => 'file',
            'attributes' => array(
                'required'               => true,
                'data-conditional-id'    => $prefix . '_img_type',
                'data-conditional-value' => 'image',
            ),
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
        ) );

        // Icon
        $cmb_image->add_field(array(
            'name' => 'Icon FontAswsome',
            'desc' => 'If "Link logo image" uploaded, icon would not display',
            'id' => $prefix . 'icon',
            'type' => 'fontawesome_icon',
            // 'default' => 'fa-link'
            'attributes' => array(
                'required'               => true,
                'data-conditional-id'    => $prefix . '_img_type',
                'data-conditional-value' => 'font_icon',
            ),
        ));

        $cmb_image->add_field(array(
            'name' => 'Icon color',
            'desc' => '',
            'id' => $prefix . 'icon_color',
            'type' => 'colorpicker',
            'default' => '#ffffff',
            'attributes' => array(
                'required'               => true,
                'data-conditional-id'    => $prefix . '_img_type',
                'data-conditional-value' => 'font_icon',
            ),
        ));

        $cmb_image->add_field(array(
            'name' => 'Icon background color',
            'desc' => '',
            'id' => $prefix . 'bg_icon',
            'type' => 'colorpicker',
            'default' => '#1e73be',
            'attributes' => array(
                'required'               => true,
                'data-conditional-id'    => $prefix . '_img_type',
                'data-conditional-value' => 'font_icon',
            ),
        ));
    //End Image type



    $cmb_general = new_cmb2_box(array(
        'id' => 'links_general',
        'title' => __('Link general', 'cmb2'),
        'object_types' => array('ulc-link'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));

    $cmb_general->add_field(array(
        'name' => __('Url', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'url',
        'type' => 'text_url',
        'attributes' => array(
            'required' => 'required',
        ),
    ));

    $cmb_general->add_field(array(
        'name' => __('Link description', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'short_descr',
        'type' => 'textarea',
    ));

    $cmb_general->add_field(array(
        'name' => __('Open link in new tab', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'is_new_tab',
        'default' => true,
        'type' => 'checkbox',
    ));

    $cmb_general->add_field(array(
        'name' => __('No follow link', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'is_nofollow',
        'type' => 'checkbox',
    ));


    $cmb_general_statistic = new_cmb2_box(array(
        'id' => 'links_statistic',
        'title' => __('Link statistic', 'cmb2'),
        'object_types' => array('ulc-link'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));

    $cmb_general_statistic->add_field(array(
        'name' => __('Clicks', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . '_stats_clicks',
        'default' => 0,
        'type'        => 'text',
        'attributes'  => array(
            'readonly' => 'readonly',
            // 'disabled' => 'disabled',
        ),
    ));

    $cmb_general_statistic->add_field(array(
        'name' => __('Likes', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . '_stats_likes',
        'default' => 0,
        'type'        => 'text',
        'attributes'  => array(
            'readonly' => 'readonly',
            // 'disabled' => 'disabled',
        ),
    ));

}

?>