<?php
// Get links Ajax
add_action('wp_ajax_ulc_get_links', 'ulc_ajax_ulc_get_links');
add_action('wp_ajax_nopriv_ulc_get_links', 'ulc_ajax_ulc_get_links');
function ulc_ajax_ulc_get_links() {
    check_ajax_referer( 'ulc-ajax-token', 'token' );
    $ret = array();

    $per_page = intval($_GET['per_page']);
    if(intval($per_page) === -1) {
        $per_page = -1;
    }
    $prefix = '_ulc_';

    $links_loop_args_get = array(
        'post_type' => !empty($_GET['post_type'])? $_GET['post_type']: 'ulc-link',
        'category_id' => !empty($_GET['category_id'])? $_GET['category_id']: '*',
        'per_page' => !empty($_GET['per_page'])? $_GET['per_page']: 10,
        'order_by' => !empty($_GET['order_by'])? $_GET['order_by']: 'title',
        'order' => !empty($_GET['order'])? $_GET['order']: 'ASC',
        'page' => !empty($_GET['page'])? $_GET['page']: 1,
        'is_likes' => !empty($_GET['is_likes']) ? $_GET['is_likes'] : false,
        'is_likes_count' => !empty($_GET['is_likes_count']) ? $_GET['is_likes_count'] : false        
    );

    $links_loop_args = array(
        'post_type' => $links_loop_args_get['post_type'],
        'post_status' => 'publish',
        'posts_per_page'  => $links_loop_args_get['per_page'],
        'orderby' => $links_loop_args_get['order_by'],
        'order' => $links_loop_args_get['order'],
        'paged' => $links_loop_args_get['page'],
        'tax_query' => array(
            array(
                'taxonomy' => 'ulc-link-type',
                'field'    => 'term_id',
                'terms'    => $links_loop_args_get['category_id'],
            ),
        ),

    );

    $links_loop = new WP_Query($links_loop_args);

    if($links_loop) {

      $prefix_link = '_ulc_link';
      while ($links_loop->have_posts()) {

          $links_loop->the_post();

          $image = wp_get_attachment_image_src( get_post_meta( get_the_ID(), $prefix_link . 'logo_img_id', true , 'ulc-thumbnail'), 'ulc-thumbnail' );
          $ret['links'][] = array(
              'id' => get_the_ID(),
              'title' => get_the_title(),
              'logo_img' => !empty($image[0]) ? $image[0] : false,
              'icon' => get_post_meta( get_the_ID(), $prefix_link . 'icon', true ),
              'icon_color' => get_post_meta( get_the_ID(), $prefix_link . 'icon_color', true ),
              'bg_icon' => get_post_meta( get_the_ID(), $prefix_link . 'bg_icon', true ),
              'description' => get_post_meta( get_the_ID(), $prefix_link . 'short_descr', true ),
              'bg_color' => get_post_meta( get_the_ID(), $prefix_link . 'bg_color', true ),
              'url' => esc_url( get_post_meta( get_the_ID(), $prefix_link . 'url', true ) ),
              'url_safe' => esc_url( home_url('/') . '?ulc_safe_link=' . get_the_ID() ),
              'is_new_tab' => get_post_meta( get_the_ID(), $prefix_link . 'is_new_tab', true ),
              'is_nofollow' => get_post_meta( get_the_ID(), $prefix_link . 'is_nofollow', true ),
          );

      }

      if($links_loop->max_num_pages > 1) {

          // Generate pagination list
          for ($i = 1; $i <= $links_loop->max_num_pages; $i++) {
              $pagination[] = array(
                'index' => $i);
          }
          $is_pagination = true;

          $ret['options'] = array(
              'page' => $links_loop_args_get['page'],
              'pagination'  => $pagination,
              'is_pagination' => $is_pagination
          );
      } else {
          $is_pagination = false;
          $ret['options'] = array(
              'is_pagination' => $is_pagination
          );
      }

      if ($links_loop_args_get['is_likes'] != false) {
          $ret['options']['is_likes'] = $links_loop_args_get['is_likes'];
      }

      if ($links_loop_args_get['is_likes'] != false && $links_loop_args_get['is_likes_count'] != false) {
          $ret['options']['is_likes_count'] = $links_loop_args_get['is_likes_count'];
      }      

      wp_reset_postdata();
      wp_reset_query();

    }

    echo json_encode($ret);
    die;

}


// Track click
add_action('wp_ajax_ulc_track_click', 'ulc_ajax_link_track_click');
add_action('wp_ajax_nopriv_ulc_track_click', 'ulc_ajax_link_track_click');

function ulc_ajax_link_track_click() {
  check_ajax_referer( 'ulc-ajax-token', 'token' );
  if(!empty($_POST['post_id'])) {
    $link_clicks = intval(get_post_meta( $_POST['post_id'], '_ulc_link_stats_clicks', true ));
    $link_clicks++;

    update_post_meta( $_POST['post_id'], '_ulc_link_stats_clicks', $link_clicks );
  }

  $response = array('Success' => true);
  echo json_encode($response);  
  die();
}


// Track click
add_action('wp_ajax_ulc_like_click', 'ulc_ajax_link_like_click');
add_action('wp_ajax_nopriv_ulc_like_click', 'ulc_ajax_link_like_click');

function ulc_ajax_link_like_click() {
  check_ajax_referer( 'ulc-ajax-token', 'token' );
  if(!empty($_POST['post_id'])) {
    $link_clicks = intval(get_post_meta( $_POST['post_id'], '_ulc_link_stats_likes', true ));
    $link_clicks++;

    update_post_meta( $_POST['post_id'], '_ulc_link_stats_likes', $link_clicks );
  }

  $response = array('Success' => true);
  echo json_encode($response);
  die();
}

?>