<?php
// Get links Ajax
function ulc_not_empty_string($s) {
  return $s !== "";
}


add_action('wp_ajax_ulc_get_links_list', 'ulc_ajax_ulc_get_links_list');
add_action('wp_ajax_nopriv_ulc_get_links_list', 'ulc_ajax_ulc_get_links_list');
function ulc_ajax_ulc_get_links_list() {
    check_ajax_referer('ulc-ajax-token', 'token');
    $lists = array();

    $per_page = intval($_GET['per_page']);
    if (intval($per_page) === -1) {
        $per_page = -1;
    }
    $prefix = '_ulc_';

    $taxs = array_filter(explode(';', $_GET['category_id']), 'ulc_not_empty_string');

    // Get taxonomy list
    foreach ($taxs as $taxonomy) {

        $ret = array();

        $tax_id = intval($taxonomy);

        $prefix_link = '_ulc_link';

        // Post clean and filter
        $links_loop_args_get = array(
            'post_type' => !empty($_GET['post_type']) ? $_GET['post_type'] : 'ulc-link',
            'category_id' => !empty($_GET['category_id']) ? $_GET['category_id'] : '*',
            'per_page' => !empty($_GET['per_page']) ? $_GET['per_page'] : 10,
            'order_by' => !empty($_GET['order_by']) ? $_GET['order_by'] : 'title',
            'order' => !empty($_GET['order']) ? $_GET['order'] : 'ASC',
            'page' => !empty($_GET['page']) ? $_GET['page'] : 1,
            'is_likes' => !empty($_GET['is_likes']) ? $_GET['is_likes'] : false,
            'is_likes_count' => !empty($_GET['is_likes_count']) ? $_GET['is_likes_count'] : false
        );

        $links_loop_args_base = array(
            'post_type' => $links_loop_args_get['post_type'],
            'post_status' => 'publish',
            'posts_per_page' => $links_loop_args_get['per_page'],
            'paged' => $links_loop_args_get['page'],
            'tax_query' => array(
                array(
                    'taxonomy' => 'ulc-link-type',
                    'field' => 'term_id',
                    'terms' => $tax_id,
                ),
            ),

        );

        if ($links_loop_args_get['order_by'] === 'clicks' || $links_loop_args_get['order_by'] === 'likes') {
            $meta_key = $prefix_link . '_stats_' . $links_loop_args_get['order_by'];
            $links_loop_args_order = array(
                'meta_key' => $meta_key,
                'orderby'  => 'meta_value_num',
            );
        } else {
            $links_loop_args_order = array(
                'orderby' => $links_loop_args_get['order_by'],
                'order'   => $links_loop_args_get['order']
            );
        }

        $links_loop_args = array_merge($links_loop_args_base, $links_loop_args_order);
        $links_loop = new WP_Query( $links_loop_args );

        if ($links_loop) {

            while ($links_loop->have_posts()) {

                $links_loop->the_post();

                $img_link = false;
                $url = get_post_meta(get_the_ID(), $prefix_link . 'url', true);
                $image = wp_get_attachment_image_src(get_post_meta(get_the_ID(), $prefix_link . 'logo_img_id', true, 'ulc-thumbnail'), 'ulc-thumbnail');
                $img_type = get_post_meta( get_the_ID(), $prefix_link . '_img_type', true );
                if ($img_type === 'favicon') {
                    $img_favicon_url = get_post_meta(get_the_ID(), $prefix_link . '_img_favicon_url', true);
                    if (!empty($img_favicon_url)) {
                        $img_link = $img_favicon_url;
                    } else {
                        $url = preg_replace('#^https?://#', '', rtrim($url,'/'));
                        $img_link = esc_url("http://favicon.yandex.net/favicon/". $url);
                    }
                } elseif (!empty($image[0]) || $img_type === 'image') {
                    $img_link = !empty($image[0]) ? $image[0] : false;
                }

                $ret['links'][] = array(
                    'id' => get_the_ID(),
                    'title' => get_the_title(),
                    'logo_img' => $img_link,
                    'icon' => get_post_meta( get_the_ID(), $prefix_link . 'icon', true ),
                    'icon_color' => get_post_meta( get_the_ID(), $prefix_link . 'icon_color', true ),
                    'bg_icon' => get_post_meta( get_the_ID(), $prefix_link . 'bg_icon', true ),
                    'description' => get_post_meta(get_the_ID(), $prefix_link . 'short_descr', true),
                    'img_type' => $img_type,
                    'bg_color' => $url,
                    'url' => esc_url(get_post_meta(get_the_ID(), $prefix_link . 'url', true)),
                    'url_safe' => esc_url(home_url('/') . '?ulc_safe_link=' . get_the_ID()),
                    'is_new_tab' => get_post_meta(get_the_ID(), $prefix_link . 'is_new_tab', true),
                    'is_nofollow' => get_post_meta(get_the_ID(), $prefix_link . 'is_nofollow', true),
                    'likes_count' => get_post_meta(get_the_ID(), $prefix_link . '_stats_likes', true)
                );

            }

            if ($links_loop->max_num_pages > 1) {
                $is_pagination = true;
                $ret['options'] = array(
                    'page' => $links_loop_args_get['page'],
                    'is_pagination' => $is_pagination,
                    'max-pages' => $links_loop->max_num_pages,
                );
            } else {
                $is_pagination = false;
                $ret['options'] = array(
                    'page' => $links_loop_args_get['page'],
                    'is_pagination' => $is_pagination,
                );
            }

            if ($links_loop_args_get['is_likes'] != false) {
                $ret['options']['is_likes'] = $links_loop_args_get['is_likes'];
            }

            if ($links_loop_args_get['is_likes'] != false && $links_loop_args_get['is_likes_count'] != false) {
                $ret['options']['is_likes_count'] = $links_loop_args_get['is_likes_count'];
            }            

            if (!empty($ret)) {
                $prefix_link_taxonomy = 'ulc_link_type_';
                $term = get_term_by('term_id', $tax_id, 'ulc-link-type');

                $lists['lists'][] = array(
                    'id' => $tax_id,
                    'links' => $ret['links'],
                    'options' => $ret['options'],
                    'icon' => get_term_meta($tax_id, $prefix_link_taxonomy . 'icon', true),
                    'icon_color' => get_term_meta($tax_id, $prefix_link_taxonomy . 'icon_color', true),
                    'bg_icon' => get_term_meta($tax_id, $prefix_link_taxonomy . 'bg_icon', true),
                    'title' => $term->name,
                    'post_count' => $term->count,
                    'slug' => $term->slug,
                    'id' => $tax_id
                );

            }

            wp_reset_query();

        }


    }
    wp_reset_postdata();

    $lists['x'] = $links_loop_args;
    echo json_encode($lists);
    die;

}

?>