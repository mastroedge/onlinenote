<?php
add_action('cmb2_admin_init', 'ulc_register_taxonomy_metabox');

function ulc_register_taxonomy_metabox()
{
    $prefix = 'ulc_link_type_';

    $cmb_term = new_cmb2_box(array(
        'id' => $prefix . 'edit',
        'title' => 'Category Metabox',
        'object_types' => array('term'),
        'taxonomies' => array('ulc-link-type'),
        'new_term_section' => true,
    ));

    $cmb_term->add_field(array(
        'name' => 'Icon FontAswsome',
        'desc' => '',
        'id' => $prefix . 'icon',
        'type' => 'fontawesome_icon',
        'default' => 'fa-link'
    ));

    $cmb_term->add_field(array(
        'name' => 'Icon color',
        'desc' => '',
        'id' => $prefix . 'icon_color',
        'type' => 'colorpicker',
        'default' => '#ffffff',
    ));

    $cmb_term->add_field(array(
        'name' => 'Icon background color',
        'desc' => '',
        'id' => $prefix . 'bg_icon',
        'type' => 'colorpicker',
        'default' => '#1e73be',
    ));
}

?>