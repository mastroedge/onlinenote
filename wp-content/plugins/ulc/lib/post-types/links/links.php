<?php

// Post type
add_action('init', 'ulc_register_post_type_links', 0);

function ulc_register_post_type_links() {

  register_post_type('ulc-link', array(
      'labels' => array(
          'name' => __('ULC Links', 'ulc'),
          'singular_name' => __('Link', 'ulc'),
          'add_new' => __('Add', 'ulc'),
          'add_new_item' => __('Add Link', 'ulc'),
          'edit_item' => __('Edit Link', 'ulc'),
          'new_item' => __('New Link', 'ulc'),
          'view_item' => __('View Link', 'ulc'),
          'search_items' => __('Search Links', 'ulc'),
          'not_found' => __('No Links found', 'ulc'),
          'not_found_in_trash' => __('No Links found in trash', 'ulc'),
          'menu_name' => __('ULC Links', 'ulc'),

      ),
      'public' => false,
      'show_ui' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => false,
      'query_var' => false,
      'supports' => array('title', 'author'),
      'menu_icon'  => 'dashicons-admin-links'
  ));

}

// Taxonomy
add_action('init', 'ulc_build_taxonomies', 0);

function ulc_build_taxonomies() {

  register_taxonomy('ulc-link-type', 'ulc-link', array(
      'hierarchical' => true,
      'labels' => array(
          'name' => __('Category', 'ulc'),
          'singular_name' => __('Category', 'ulc'),
          'search_items' => __('Search Category', 'ulc'),
          'all_items' => __('All Category', 'ulc'),
          'parent_item' => __('Parent Category', 'ulc'),
          'parent_item_colon' => __('Parent Category:', 'ulc'),
          'edit_item' => __('Edit Category', 'ulc'),
          'update_item' => __('Оновити Категорію', 'ulc'),
          'add_new_item' => __('Add Category', 'ulc'),
          'new_item_name' => __('New Category', 'ulc'),
          'menu_name' => __('Category', 'ulc'),
      ),
      'query_var' => true,
      'hierarchical' => false,
      'rewrite' => true
    )

  );
}

?>