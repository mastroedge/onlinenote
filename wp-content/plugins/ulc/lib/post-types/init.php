<?php
// Init all post types & metaboxes
require_once( ULC_PLUGIN_DIR . 'lib/post-types/links/links.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/links/links-metaxboxes.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/links/links-term-metaxbox.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/links/links-ajax.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/links/links-list-ajax.php' );

require_once( ULC_PLUGIN_DIR . 'lib/post-types/ulc-shortcode/ulc-shortcode.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/ulc-shortcode/ulc-shortcode-metaboxes.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/ulc-shortcode/admin-table.php' );
?>