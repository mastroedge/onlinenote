<?php
// Admin
// add_action( 'admin_enqueue_scripts', 'ulc_backend_files' );
function ulc_backend_files() {
  if (is_admin()) {
    wp_register_style( 'ulc_backend-main', ULC_PLUGIN_URL . 'assets/admin/css/style.css', array(), ULC_VERSION, 'all' );
    wp_enqueue_style( 'ulc_backend-main' );
  }
}

// Frontend
add_action( 'wp_enqueue_scripts', 'ulc_frontend_files' );

function ulc_frontend_files() {
  // Scripts
  wp_register_script( 'jquery.webui-popover', ULC_PLUGIN_URL . 'assets/js/plugins/jquery.webui-popover.min.js', array(), ULC_VERSION, true );
  wp_register_script( 'ulc_masonry', ULC_PLUGIN_URL . 'assets/js/plugins/masonry.pkgd.min.js', array(), ULC_VERSION, true );
  wp_register_script( 'mustache', ULC_PLUGIN_URL . 'assets/js/plugins/mustache.js', array(), ULC_VERSION, true );
  wp_register_script( 'ulc_main', ULC_PLUGIN_URL . 'assets/js/main.js', array('jquery', 'mustache', 'ulc_masonry', 'jquery.webui-popover'), ULC_VERSION, false );

  $ulc_settings_obj = array(
    'ajaxurl' => admin_url("admin-ajax.php"),
    'homeUrl' => esc_url( home_url( '/' ) ),
    'token' => wp_create_nonce( "ulc-ajax-token" ),
    'pluginUrl' => esc_html( ULC_PLUGIN_URL )
  );
  wp_localize_script('ulc_main', 'ulcSettingsObj', $ulc_settings_obj);


  // Styles
  wp_register_style( 'font-awesome', ULC_PLUGIN_URL . 'assets/css/font-awesome.min.css', array(), ULC_VERSION, 'all' );
  wp_register_style( 'jquery.webui-popover', ULC_PLUGIN_URL . 'assets/css/jquery.webui-popover.min.css', array(), ULC_VERSION, 'all' );
  wp_register_style( 'ulc_main', ULC_PLUGIN_URL . 'assets/css/style.css', array('jquery.webui-popover'), ULC_VERSION, 'all' );

  wp_enqueue_style( 'font-awesome' );
  wp_enqueue_style( 'ulc_main' );
}

// Custom image size
add_image_size('ulc-thumbnail', 80, 80, true);


// Safe redirect
add_action( 'template_redirect', 'ulc_safe_redirect' );

function ulc_safe_redirect() {
  if(!empty($_GET['ulc_safe_link'])) {
    $get_id = intval($_GET['ulc_safe_link']);
    if (!empty($get_id)) {
        $prefix_link = '_ulc_link';
        $location = esc_url( get_post_meta( $get_id, $prefix_link . 'url', true ) );
        if(!empty($location)) {
            wp_redirect( $location );
        } else {
          wp_redirect( home_url('/') );
        }
    }
  }
}

?>