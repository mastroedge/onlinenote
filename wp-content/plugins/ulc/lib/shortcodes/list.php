<?php
// Shortocode
function ulc( $atts, $content = '', $id = '' ) {
  extract( shortcode_atts( array(
    'id'  => null,
    'taxs'  => '*',
    'posts_per_page'  => '-1',
    'order_by'  => 'title',
    'order' => 'ASC',
    'cols'  => '3',
    'layout_style'  => 'default',
    'is_filter' => 'on',
    'is_search_bar' => 'on',
    'is_likes' => false,
    'is_likes_count' => false
  ), $atts ) );

  wp_enqueue_script('ulc_main');
  $search_bar = false;
  $attributes = '';
  $ulc_id = '';
  $ulc_id_val = '';
  if(!empty($atts['id'])) {
    $ulc_id_val = trim($atts['id']);
    $ulc_id = ' id="ulc-' . $ulc_id_val . '"';
  }
  if(!empty($atts['taxs'])) {
    if($atts['taxs'] === "*") {
      $term_list = array();
      $tax_ids = get_terms('ulc-link-type');
      foreach ($tax_ids as $term) {
        $term_list[] = $term->term_id;
      }
      $attributes .= ' data-taxs="' . implode(";", $term_list) . '"';
    } else {
      $attributes .= ' data-taxs="' . $atts['taxs'] . '"';
    }
  }
  if(!empty($atts['posts_per_page'])) {
    $attributes .= ' data-posts-per-page="' . $atts['posts_per_page'] . '"';
  }

  if(!empty($atts['posts_per_page']) && intval($atts['posts_per_page']) === -1
    && !empty($atts['is_search_bar']) && $atts['is_search_bar'] === 'on') {
    $search_bar = true;
  }
  if(!empty($atts['order_by'])) {
    $attributes .= ' data-order-by="' . $atts['order_by'] . '"';
  }
  if(!empty($atts['order'])) {
    $attributes .= ' data-order="' . $atts['order'] . '"';
  }

  if(!empty($atts['layout_style'])) {
    $attributes .= ' data-layout-style="' . $atts['layout_style'] . '"';
    $layoutStyle = ' class="ulc-grid ulc-style-' . $atts['layout_style'] . ' ulc-grid-columns-' . $atts['cols'] . '"';
  }
  $output = '';
  $output .= '<div' . $ulc_id  . $layoutStyle . '>';


  if(!empty($atts['is_likes']) && $atts['is_likes'] === 'on' ) {
    $attributes .= ' data-is-likes="' . $atts['is_likes'] . '"';
  }

  if(!empty($atts['is_likes_count']) && $atts['is_likes_count'] === 'on' ) {
    $attributes .= ' data-is-likes-count="' . $atts['is_likes_count'] . '"';
  }  

  // Search bar
  if($search_bar) {
  $output .= '    <div class="ulc-search-bar">';
  $output .= '      <div class="ulc-search-bar-control">';
  $output .= '        <input type="text" id="ulc-search-' . $ulc_id_val . '" class="ulc-search ulc-form-control" placeholder="Search">';
  $output .= '        <i class="fa fa-search ulc-icon-search" aria-hidden="true"></i>';
  $output .= '        <i class="js-search-input-remove ulc-icon-remove fa fa-times-circle" aria-hidden="true"></i>';
  $output .= '      </div>';
  $output .= '    </div>';
  }
  // End Search bar

  // Filter
  if(!empty($atts['is_filter']) && $atts['is_filter'] === 'on') {
    if (!empty($atts['taxs'])) {
      $tax_ids = explode(";", $atts['taxs']);
      if(count($tax_ids) > 1 || $atts['taxs'] === "*") {
        $output .='  <div class="js-ulc-filter-btns  ulc-filter">';
        $output .='    <button class="ulc-btn ulc-btn-default js-ulc-filter-btn" data-filter="*">All</button>';
        if($atts['taxs'] === "*") {
          $tax_ids = get_terms('ulc-link-type');
          foreach ($tax_ids as $term) {
              $output .= '<button class="ulc-btn ulc-btn-default js-ulc-filter-btn" data-filter="' . $term->term_id . '">' . $term->name . '</button>';
          }
        } else {
          foreach ($tax_ids as $tax_id) {
              $term = get_term_by('term_id', $tax_id, 'ulc-link-type');
              $output .= '<button class="ulc-btn ulc-btn-default js-ulc-filter-btn" data-filter="' . $term->term_id . '">' . $term->name . '</button>';
          }
        }
        $output .= '  </div>';
      }
      wp_reset_postdata();
    }
  }
  // End Filter

  $output .= '<div class="ulc-list-render"' . $attributes . '>';

  // Loader
  $output .= '    <div class="ulc-loader-overlay"></div>';
  $output .= '    <div class="ulc-loader">';
  $output .= '        <div class="ulc-circle ulc-circle-1"></div>';
  $output .= '        <div class="ulc-circle ulc-circle-2"></div>';
  $output .= '        <div class="ulc-circle ulc-circle-3"></div>';
  $output .= '    </div>';
  // End Loader

  $output .= '</div>';
  $output .= '</div>';
  return $output;


}
add_shortcode( 'ulc', 'ulc' );

?>