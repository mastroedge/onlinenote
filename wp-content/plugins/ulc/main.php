<?php
/*
Plugin Name: Useful Link Collections
Plugin URI:
Description: Plugin allow you to create useful links collection or favorite bookmarks and share links list with visitors.
Version: 1.1.8
Author: mzworks
Author URI: https://codecanyon.net/user/mzworks/
*/

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );

define( 'ULC_VERSION', '1.1.8' );
define( 'ULC_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'ULC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


// Plugin init
require_once( ULC_PLUGIN_DIR . 'lib/shortcodes/init.php' );
require_once( ULC_PLUGIN_DIR . 'lib/post-types/init.php' );
require_once( ULC_PLUGIN_DIR . 'lib/plugins/init.php' );
require_once( ULC_PLUGIN_DIR . 'lib/admin/init.php' );
require_once( ULC_PLUGIN_DIR . 'lib/functions.php' );

?>