(function($) {
      'use strict';

      // Chart
      if (ulc_stat_data_labels && ulc_stat_data_clicks && ulc_stat_data_likes) {
            var ctx = document.getElementById('ulc-click-chart').getContext('2d');
            var myChart = new Chart(ctx, {
                  type: 'line',
                  data: {
                        labels: ulc_stat_data_labels,
                        datasets: [{
                              label: 'Clicks',
                              data: ulc_stat_data_clicks,
                              backgroundColor: "rgba(52, 152, 219, 0.1)",
                              borderColor: "#3498db",
                              borderCapStyle: 'butt',
                              borderDash: [],
                              borderDashOffset: 0.0,
                              borderJoinStyle: 'miter',
                              pointBorderColor: "#3498db",
                              pointBackgroundColor: "#fff",
                              pointBorderWidth: 1,
                              pointHoverRadius: 5,
                              pointHoverBackgroundColor: "#3498db",
                              pointHoverBorderColor: "rgba(220,220,220,1)",
                              pointHoverBorderWidth: 2,
                              pointRadius: 5,
                              pointHitRadius: 10,
                        }, {

                              label: 'Likes',
                              data: ulc_stat_data_likes,
                              backgroundColor: "rgba(230, 126, 34, 0.1)",
                              borderColor: "#e67e22",
                              borderCapStyle: 'butt',
                              borderDash: [],
                              borderDashOffset: 0.0,
                              borderJoinStyle: 'miter',
                              pointBorderColor: "#e67e22",
                              pointBackgroundColor: "#fff",
                              pointBorderWidth: 1,
                              pointHoverRadius: 5,
                              pointHoverBackgroundColor: "#e67e22",
                              pointHoverBorderColor: "rgba(220,220,220,1)",
                              pointHoverBorderWidth: 2,
                              pointRadius: 5,
                              pointHitRadius: 10,
                        }]
                  },

            });
      }

      // Sort table
      var sortTableoptions = {
            valueNames: ['number', 'title', 'clicks', 'likes']
      };

      var userList = new List('ulc-table-links', sortTableoptions);

      // Reset stat
      function ulcResetAllStat(confirm) {
            if (confirm === true) {
                  $.ajax({
                        type: 'POST',
                        url: ulcAdminObj.ajaxurl,
                        dataType: 'json',
                        data: {
                              action: 'ulc_links_stat_reset',
                              token: ulcAdminObj.token
                        },
                        success: function(data) {
                              location.reload();
                              console.log(data);
                        }
                  });
            }

      }

      $(document).on('click', '#js-reset-stat', function(e) {
            var confirm = window.confirm("Are you sure you want to reset all statistic for Likes and Clicks?");
            if (confirm) {
                  ulcResetAllStat(confirm);
            }
            e.preventdefault;
      });
})(jQuery);