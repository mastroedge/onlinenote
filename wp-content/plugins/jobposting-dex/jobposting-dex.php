<?php
/**
* Plugin Name: JobPosting-Dex
* Plugin URI: http://themeforest.net/user/dexthemes
* Description: Create a custom portfolio
* Version: 1.0.0
* Author: Dexthemes
* Author URI: http://themeforest.net/user/dexthemes
**/


 



/*--- Creating custom post type for portfolio --*/
add_action('init', 'g_portfolio_custom_init');

/*-- Custom Post Init Begin --*/
function g_portfolio_custom_init()
{
  $labels = array(
    'name' => 'Job Posts',
    'singular_name' => 'Job Post',
    'add_new' => 'Add New', 'jobpost',
    'add_new_item' => ('Add New Job Post'),
    'edit_item' => ('Edit Job Post'),
    'new_item' => ('New Job Post'),
    'view_item' => ('View Job Post'),
    'search_items' => ('Search Job Post'),
    'not_found' =>  ('No Job Post found'),
    'not_found_in_trash' => ('No Job Post found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'Job Posts' ,	
  );
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    //'hierarchical' => false,
    'menu_position' => null,	
    'supports' => array('title','editor','author','thumbnail','comments')
  );
  register_post_type('jobpost',$args);
}







