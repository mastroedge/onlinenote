<?php
$imagedir = get_template_directory_uri().'/images/';

if(r_option('logo_panel')!=''){
$logo_panel = r_option('logo_panel');
}else{
$logo_panel = $imagedir.'logo-panel.png';
}
return array(
	'title' => __('Online Note Option Panel', 'onotes'),
	'logo' => $logo_panel,
	'menus' => array(
		array(
			'title' => __('General Settings', 'onotes'),
			'name' => 'general',
			'icon' => 'font-awesome:fa-home',
			'menus' => array(
				array(
					'title' => __('Logo', 'onotes'),
					'name' => 'logo',
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'logo_img',
							'label' => __('Logo', 'onotes'),
							'description' => __('Upload your own logo .', 'onotes'),
							'default' => $imagedir . 'logo.png',
						),array(
							'type' => 'textbox',
							'name' => 'logo_text',
							'label' => __('Logo Text', 'onotes'),
							'description' => __('The text of the logo .', 'onotes'),
							'default' => 'Online Note',
						),array(
							'type' => 'upload',
							'name' => 'logo_alt_retina',
							'label' => __('Alternative Logo Image (Retina Version)', 'onotes'),
							'description' => __('Please name your file following the  (e.g. logo-alt.png) with a suffix @2x (e.g. logo-alt@2x.png)', 'onotes'),
						),array(
							'type' => 'upload',
							'name' => 'logo_panel',
							'label' => __('Logo for this admin pagel', 'onotes'),
							'description' => __('Logo for this admin pagel', 'onotes'),
							'default' => $imagedir . 'logo-panel.png',
						),array(
							'type' => 'slider',
							'name' => 'logo_height',
							'label' => __('Logo height (in pixels)', 'onotes'),
							'description' => __('Choose height for the Logo', 'onotes'),
							'default' => 26,
						),
					),
				),
				array(
					'title' => __('General', 'onotes'),
					'name' => 'general',
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(						
						array(
							'type' => 'toggle',
							'name' => 'responsive_mode',
							'label' => __('Responsive/mobile design', 'onotes'),
							'description' => __('Enable this to adapt your page to differents screen sizes(mobile, tablet, desktop, etc).', 'onotes'),
							'default' => '1',
						),
						array(
							'type' => 'textbox',
							'name' => 'feedburner',
							'label' => __('Feedburner URL', 'onotes'),
							'description' => __('Enter your full FeedBurner URL to replace the default feed URL by Wordpress.', 'onotes'),
							'default' => '',
						),
						array(
							'type' => 'textarea',
							'name' => 'analyticscode',
							'label' => __('Analytics Code', 'onotes'),					
							'default' => '',
						)
					),
				),

				array(
					'type' => 'section',
					'title' => __('Custom Fonts', 'onotes'),
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(

                        array(
                            'label' => __('Header Font Family url', 'onotes'),
                            'description' => __('Enter the url of the Google font you want for header font. ej: http:&#47;&#47;fonts.googleapis.com/css?family=Roboto:400,300,600,700', 'onotes'),
                            'name'=>'font_family_url2',
                            'type' => 'textbox',
                            'default' => "https://fonts.googleapis.com/css?family=Rajdhani:400,500,600,700,300"
                        ),array(
                            'label' => __('Header Font Family name', 'onotes'),
                            'description' => __('Enter the declaration (name) of your google font for header font.', 'onotes'),
                            'name'=>'font_family_name2',
                            'type' => 'textbox',
                            'default' => "Rajdhani"
                        ),

						array(
							'label' => __('Body Font Family url', 'onotes'),
							'description' => __('Enter the url of the Google font you want for body. ej: http:&#47;&#47;fonts.googleapis.com/css?family=Roboto:400,300,600,700', 'onotes'),
							'name'=>'font_family_url1',
							'type' => 'textbox',
							'default' => "https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700"
						),array(
							'label' => __('Body Font Family name', 'onotes'),
							'description' => __('Enter the declaration of your google font.', 'onotes'),
							'name'=>'font_family_name1',
							'type' => 'textbox',
							'default' => "Raleway"
						)
					),
				),
				array(
					'type' => 'section',
					'title' => __('Site Favicon', 'onotes'),
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(
						array(
							'type' => 'upload',
							'name' => 'favicon',
							'description' => __('Recommended: .ICO 64x64px size', 'onotes'),
							'label' => __('General Site Icon', 'onotes'),
							'default' => $imagedir . 'favicon.ico',
						),
						array(
							'type' => 'upload',
							'name' => 'favicon_iphone',
							'description' => __('Recommended: .PNG 60x60px size', 'onotes'),
							'label' => __('Icon for iPhone', 'onotes'),
							'default' => $imagedir . 'apple-touch-icon.png',
						),
						array(
							'type' => 'upload',
							'name' => 'favicon_iphone_retina',
							'description' => __('Recommended: .PNG 120x120px size', 'onotes'),
							'label' => __('Icon for iPhone Retina', 'onotes'),
							'default' => $imagedir . 'apple-touch-icon-114x114.png',
						),
						array(
							'type' => 'upload',
							'name' => 'favicon_ipad',
							'description' => __('Recommended: .PNG 76x76px size', 'onotes'),
							'label' => __('Icon for iPad', 'onotes'),
							'default' => $imagedir . 'apple-touch-icon-72x72.png',
						),
						array(
							'type' => 'upload',
							'name' => 'favicon_ipad_retina',
							'description' => __('Recommended: .PNG 152x152px size', 'onotes'),
							'label' => __('Icon for iPad Retina', 'onotes'),
						),
					),
				),

				array(
					'title' => __('Color Scheme', 'onotes'),
					'name' => 'color_scheme',
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(
						array(
							'type' => 'toggle',
							'name' => 'header_white',
							'label' => __('Enable a boxed style, with header white, and widget boxes', 'onotes'),
							'description' => __('Enable a boxed style.', 'onotes'),
							'default' => '0',
						),
						array(
							'type' => 'toggle',
							'name' => 'custom_color',
							'label' => __('Enable Custom Color', 'onotes'),
							'description' => __('Set your custom color scheme.', 'onotes'),
							'default' => '1',
						),
                        array(
							'name'      => 'color_pri',
							'label'     => __('Select Color', 'onotes'),
							'type' => 'select',
							'description' => __('Select Color Scheme', 'onotes'),
							'items' => array(
								array(
									'value' => 'lime',
									'label' => __('lime', 'onotes'),
								),array(
									'value' => 'alizarin',
									'label' => __('alizarin', 'onotes'),
								),array(
									'value' => 'green-sea',
									'label' => __('green-sea', 'onotes'),
								),array(
									'value' => 'nephritis',
									'label' => __('nephritis', 'onotes'),
								),array(
									'value' => 'anethyst',
									'label' => __('anethyst', 'onotes'),
								),array(
									'value' => 'peter-river',
									'label' => __('peter-river', 'onotes'),
								)
							),
							'default' => array(
								'lime',
							),
						),
					),	
				),


				array(
					'title' => __('404 Page', 'onotes'),
					'name' => '404page',
					'icon' => 'font-awesome:fa-angle-right',
					'controls' => array(
						array(
							'type' => 'textbox',
							'name' => '404_title',
							'label' => __('Page Not Found Title', 'onotes'),
							'default' => 'PAGE NOT FOUND',
						),
					),	
				),
			),
		),
		
		array(
			'type' => 'section',
			'title' => __('Title Labels', 'onotes'),
			'icon' => 'font-awesome:fa-credit-card',
			'controls' => array(
				array(
					'type' => 'textbox',
					'name' => 'label_blog',
					'label' => __('Blog label', 'onotes'),
					'default' => 'OUR BLOG',
				),array(
					'type' => 'textbox',
					'name' => 'label_category',
					'description' => __('%s is for the category name', 'onotes'),
					'label' => __('Post Category Archive Title', 'onotes'),
					'default' => 'Notes in Category: %s',
				),
				array(
					'type' => 'textbox',
					'name' => 'label_tag',
					'description' => __('%s is for the tag name', 'onotes'),
					'label' => __('Post Tag Archive Title', 'onotes'),
					'default' => 'Notes Tagged Under: %s',
				),
				array(
					'type' => 'textbox',
					'name' => 'label_time_year',
					'description' => __('%s is for the year', 'onotes'),
					'label' => __('Yearly Archive Title', 'onotes'),
					'default' => 'Notes in: %s',
				),
				array(
					'type' => 'textbox',
					'name' => 'label_time_month',
					'description' => __('%s is for the month', 'onotes'),
					'label' => __('Monthly Archive Title', 'onotes'),
					'default' => 'Notes in: %s',
				),
				array(
					'type' => 'textbox',
					'name' => 'label_time_day',
					'description' => __('%s is for the day', 'onotes'),
					'label' => __('Daily Archive Title', 'onotes'),
					'default' => 'Notes in: %s',
				),
				array(
					'type' => 'textbox',
					'name' => 'label_search',
					'description' => __('First %s is for total results found, the second %s is for the query', 'onotes'),
					'label' => __('Search Result Title', 'onotes'),
					'default' => 'Search Results for: %s',
				),array(
					'type' => 'textbox',
					'name' => 'label_job',
					'description' => __('Label for Job Notes archive', 'onotes'),
					'label' => __('Label for Job Notes archive', 'onotes'),
					'default' => 'Reader\'s Job Board.',
				),array(
					'type' => 'textbox',
					'name' => 'read_more_label',
					'description' => __('Label for the button "Read More"', 'onotes'),
					'label' => __('Label for the button "Read More"', 'onotes'),
					'default' => 'Read More',
				)
				,array(
					'type' => 'textbox',
					'name' => 'jobboard_s',
					'description' => __('Overwrite "Job board" (after installed jobpostingdex plugin)', 'onotes'),
					'label' => __('Overwrite "Job board" ', 'onotes'),
					'default' => 'Job Post',
				)
				,array(
					'type' => 'textbox',
					'name' => 'jobboard_p',
					'description' => __('Overwrite "Job board " (plural - after installed jobpostingdex plugin)', 'onotes'),
					'label' => __('Overwrite "Job board" (plural)', 'onotes'),
					'default' => 'Job Posts',
				)
				,array(
					'type' => 'textbox',
					'name' => 'jobboard_slug',
					'description' => __('Overwrite "Job board " slug (after installed jobpostingdex plugin) *WITHOUT SPACES', 'onotes'),
					'label' => __('Overwrite "Job board" slug', 'onotes'),
					'default' => 'jobpost',
				)
			),
		),
		
		array(
			'type' => 'section',
			'title' => __('Blog/Archive Settings', 'onotes'),
			'icon' => 'font-awesome:fa-file-text',
			'controls' => array(
				array(
					'name' => 'style_blog',
					'label' => __('Blog Style', 'onotes'),
					'description' => __('Choose The blog style', 'onotes'),
					'type' => 'radiobutton',
					'items' => array(
						array(
							'value' => 'classic',
							'label' => __('Classic timeline', 'onotes'),
						),
						array(
							'value' => 'grid',
							'label' => __('Grid', 'onotes'),
						)						
					),
					'default' => array(
						'grid',
					),
				),

                array(
                    'name'      => 'select-layout',
                    'label'     => __('Select Layout', 'onotes'),
                    'type' => 'select',
                    'description' => __('Select Blog Layout', 'onotes'),
                    'items' => array(
                        array(
                            'value' => 'container-fluid',
                            'label' => __('Full width', 'onotes'),
                        ),array(
                            'value' => 'container',
                            'label' => __('Box', 'onotes'),
                        ),
                    ),
                    'default' => array(
                        'container-fluid',
                    ),
                ),

                array(
                    'name'      => 'select-style',
                    'label'     => __('Select Style', 'onotes'),
                    'type'      => 'select',
                    'description' => __('Select Blog Style', 'onotes'),
                    'items' => array(
                        array(
                            'value' => 'style-1',
                            'label' => __('Style 1', 'onotes'),
                        ),array(
                            'value' => 'style-2',
                            'label' => __('Style 2', 'onotes'),
                        ),
                    ),
                    'default' => array(
                        'style-1',
                    ),
                ),

				/*array(
					'name' => 'sidebar_s',
					'label' => __('Sidebar placement', 'onotes'),
					'description' => __('Choose if you want to show a Sidebar at the right side, or two at both sides', 'onotes'),
					'type' => 'radiobutton',
					'items' => array(
						array(
							'value' => 'both_s',
							'label' => __('Both sidebar', 'onotes'),
						),
						array(
							'value' => 'right_s',
							'label' => __('Right', 'onotes'),
						)						
					),
					'default' => array(
						'right_s',
					),
				),*/
				array(
					'type' => 'toggle',
					'name' => 'ajax_posts',
					'label' => __('Load Post with ajax', 'onotes'),
					'description' => __('Load Post when scroll down, this option will disable the navigation', 'onotes'),
					'default' => '1',
				),
				array(
					'name'=>'word_limit',
					'type' => 'textbox', 
					'label' => __('Word Limit Excerpt', 'onotes'),
					'description' => __('Set a limit number of words for each excerpt in blog.', 'onotes'),
					'default' => '130',
				),array(
					'name'=>'word_limit_grid',
					'type' => 'textbox', 
					'label' => __('Word Limit Excerpt (grid)', 'onotes'),
					'description' => __('Set a limit number of words for each excerpt in blog (for grid style).', 'onotes'),
					'default' => '50',
				),array(
					'type' => 'toggle',
					'name' => 'auto_thumbnail',
					'label' => __('Show Post Thumbnail automaticaly', 'onotes'),
					'description' => __('Show Post Thumbnail automaticaly, if not, you can place your thumbnail using this shortcode []', 'onotes'),
					'default' => '1',
				),array(
					'type' => 'toggle',
					'name' => 'uppertitle_onoff',
					'label' => __('Uppercase Title Post', 'onotes'),
					'default' => '0',
				),array(
					'type' => 'textbox',
					'name' => 'n_load',
					'label' => __('Load Post with ajax(clasic)', 'onotes'),
					'description' => __('if Load Post Ajax is enabled, set a limit to load posts', 'onotes'),
					'default' => '3',
				),array(
					'type' => 'textbox',
					'name' => 'n_load_g',
					'label' => __('Load Post with ajax(grid)', 'onotes'),
					'description' => __('If Load Post Ajax is enabled, set a limit to load posts', 'onotes'),
					'default' => '4',
				),array(
					'type' => 'toggle',
					'name' => 'show_read_more',
					'label' => __('Show Read More Button in Classic Blog', 'onotes'),
					'description' => __('turn on if you want to show the Read Button in the classic blog style.', 'onotes'),
					'default' => '0',
				)
				
			),
		),
		
		array(
			'type' => 'section',
			'title' => __('Single Post', 'onotes'),
			'icon' => 'font-awesome:fa-file-text-o',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'uppertitle_onoff',
					'label' => __('Uppercase Title Post', 'onotes'),
					'default' => '0',
				),				
				array(
					'name' => 'meta_posts',
					'label' => __('Meta Elements in post', 'onotes'),
					'description' => __('Choose the meta elements to show in each posts', 'onotes'),
					'type' => 'checkbox',
					'items' => array(
						array(
							'value' => 'user',
							'label' => __('User', 'onotes'),
						),
						array(
							'value' => 'date',
							'label' => __('Date', 'onotes'),
						),
						array(
							'value' => 'category',
							'label' => __('Category', 'onotes'),
						),
						array(
							'value' => 'comments',
							'label' => __('Comments', 'onotes'),
						),			
					),
					'default' => array(
						'user',
						'date',
						'category',
						'comments',
					),
				),array(
					'type' => 'toggle',
					'name' => 'show_bio',
					'label' => __('Show Biography in post', 'onotes'),
					'default' => '1',
				)
			),
		),
		array(
			'title' => __('Pages', 'onotes'),
			'name' => 'pages',
			'icon' => 'font-awesome:fa-code',
			'controls' => array(
				array(
					'type' => 'toggle',
					'name' => 'style_page',
					'label' => __('Centered Header for pages', 'onotes'),
					'description' => __('turn on if you want the header in pages be centered; turn off and the page will look like the general design', 'onotes'),
					'default' => '1',
				),array(
					'type' => 'toggle',
					'name' => 'sidebar_left_page',
					'label' => __('Show Left Sidebar in Page', 'onotes'),
					'description' => __('turn on if you want to show the left sidebar in pages, turn off to show only the right sidebar', 'onotes'),
					'default' => '0',
				),array(
					'type' => 'toggle',
					'name' => 'comments_page',
					'label' => __('Show Comments section in Page', 'onotes'),
					'description' => __('turn on if you want to enable comments in pages.', 'onotes'),
					'default' => '0',
				)
			),
		),
		
		
				
				
				
				
		array(
			'type' => 'section',
            'title' => __('Share Post', 'onotes'),
            'icon' => 'font-awesome:fa-share-square-o',
            'controls' => array(
				array(
					'label' => __('Facebook', 'onotes'),
					'name'=>'facebook_share',
					'type' => 'toggle', 
					"default" => 1,
				),array(
					'label' => __('Twitter', 'onotes'),
					'name'=>'twitter_share',
					'type' => 'toggle', 
					"default" => 1,
				),array(
					'label' => __('Linkedin', 'onotes'),
					'name'=>'linkedin_share',
					'type' => 'toggle', 
					"default" => 0,
				),array(
					'label' => __('Pinterest', 'onotes'),
					'name'=>'pinterest_share',
					'type' => 'toggle', 
					"default" => 1,
				),array(
					'label' => __('GooglePlus', 'onotes'),
					'name'=>'googlep_share',
					'type' => 'toggle', 
					"default" => 1,
				),array(
					'label' => __('Reddit', 'onotes'),
					'name'=>'reddit_share',
					'type' => 'toggle', 
					"default" => 0,
				),array(
					'label' => __('Mail', 'onotes'),
					'name'=>'mail_share',
					'type' => 'toggle', 
					"default" => 0,
				),array(
					'label' => __('Tumblr', 'onotes'),
					'name'=>'tumblr_share',
					'type' => 'toggle', 
					"default" => 0,
				)
			),
		),

		array(
			'title' => __('Footer', 'onotes'),
			'icon' => 'font-awesome:fa-building',
			'controls' => array(	
				array(
					'name'=>'copyrights',
					'type' => 'textarea', 
					'label' => __('Copyrights', 'onotes'),
					'description' => __('', 'onotes'),
					'default' => 'All Right Reserved &copy; <a href="https://www.onlinenote.in">Online Note</a>'
				),
				array(
					'label' => __('Footer Style', 'onotes'), 
					'description' => __('Choose how you want to order your portfolio items', 'onotes'),
					'name'=>'footer_style',
					'type' => 'select',
					'items' => array(
						array(
							'value' => 'bluish-dark',
							'label' => __('Dark', 'onotes'),
						),
						array(
							'value' => 'light',
							'label' => __('Light', 'onotes'),
						),
					),
					'default' => array(
						'light',
					),
				),
				array(
					'type' => 'slider',
					'name' => 'width_foo1',
					'label' => __('Width footer widget 1', 'onotes'),
					'min' => '0',
					'max' => '24',
					'step' => '1',
					'default' => '9',
				),
				array(
					'type' => 'slider',
					'name' => 'width_foo2',
					'label' => __('Width footer widget 2', 'onotes'),
					'min' => '0',
					'max' => '24',
					'step' => '1',
					'default' => '6',
				),
				array(
					'type' => 'slider',
					'name' => 'width_foo3',
					'label' => __('Width footer widget 3', 'onotes'),
					'min' => '0',
					'max' => '24',
					'step' => '1',
					'default' => '9',
				),				
			),
		),
		array(
			'title' => __('Custom Codes', 'onotes'),
			'name' => 'custom_codes',
			'icon' => 'font-awesome:fa-code',
			'controls' => array(
				array(
					'type' => 'codeeditor',
					'name' => 'custom_css',
					'label' => __('Custom CSS', 'onotes'),
					'description' => __('Write your custom css here.', 'onotes'),
					'theme' => 'github',
					'mode' => 'css',
				),
				array(
					'type' => 'codeeditor',
					'name' => 'custom_js',
					'label' => __('Custom JavaScript', 'onotes'),
					'description' => __('Write your custom js here.', 'onotes'),
					'theme' => 'twilight',
					'mode' => 'javascript',
				),
			),
		),
		
    ),

);
?>