<?php
/**
 * Header bar template part.
 *
 * @package AMP
 */

?>
<header id="top" class="amp-wp-header">
	<div>
		<a href="<?php echo esc_url( $this->get( 'home_url' ) ); ?>">
		<!-- LOGO -->
        <?php if(file_exists(get_template_directory() .'/images/onlinenote-logo.png')) { ?>
        	<amp-img src="<?php echo get_bloginfo('template_url').'/images/onlinenote-logo.png'; ?>" alt="<?php bloginfo('name'); ?>" width="196" height="53" class="amp-wp-site-icon"></amp-img>
        <?php } else { ?>
        	<amp-img src="<?php echo esc_url($logo_img); ?>" alt="<?php bloginfo('name'); ?>" width="196" height="53" class="amp-wp-site-icon"></amp-img>
        <?php } ?>
		
		<?php /*?>
			<?php $site_icon_url = $this->get( 'site_icon_url' ); ?>
			<?php if ( $site_icon_url ) : ?>
				<amp-img src="<?php echo esc_url( $site_icon_url ); ?>" width="32" height="32" class="amp-wp-site-icon"></amp-img>
			<?php endif; ?>
			<span class="amp-site-title">
				<?php echo esc_html( wptexturize( $this->get( 'blog_name' ) ) ); ?>
			</span>
			*/ ?>
		</a>
	</div>
</header>
