<?php
require_once 'framework/vafpress/bootstrap.php';
require_once "include/Mobile_Detect.php";
function isMobile() {
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
}
$r_widget_rp = get_template_directory() . '/widgets/recent-post.php';
include $r_widget_rp;

$r_widget_pp = get_template_directory() . '/widgets/popular-post.php';
include $r_widget_pp;

$r_widget_tf = get_template_directory() . '/widgets/twitter-feed.php';
include $r_widget_tf;

$r_widget_si = get_template_directory() . '/widgets/social-icons.php';
include $r_widget_si;

$r_widget_ad = get_template_directory() . '/widgets/advertisements.php';
include $r_widget_ad;

$r_widget_au = get_template_directory() . '/widgets/about-us.php';
include $r_widget_au;

$r_widget_jp = get_template_directory() . '/widgets/job-posts.php';
include $r_widget_jp;

require_once (get_template_directory() . '/include/wp_bootstrap_navwalker.php');
include (get_template_directory() . '/include/helper.php');

function r_get_image_id($image_url)
{
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
    if (isset($attachment[0])) {
        return $attachment[0];
    }
}

/**
 * Building Options
 */
function r_init_options()
{
    global $theme_options;
    $r_option_path = get_template_directory() . '/framework/options/option.php';
    $theme_options = new VP_Option(array(
        'is_dev_mode' => false,
        'option_key' => 'r_option',
        'page_slug' => 'r_option',
        'template' => $r_option_path,
        'menu_page' => array(
            'position' => '100.4'
        ),
        'use_auto_group_naming' => true,
        'use_exim_menu' => false,
        'use_util_menu' => true,
        'minimum_role' => 'edit_theme_options',
        'layout' => 'fixed',
        'page_title' => __('Theme Options', 'onotes'),
        'menu_label' => __('Theme Options', 'onotes')
    ));
}
add_action('after_setup_theme', 'r_init_options');

function r_option($key)
{
    return vp_option('r_option' . '.' . $key);
}
if (! isset($content_width))
    $content_width = 600;

// directory of languages folder
load_theme_textdomain('onotes', get_stylesheet_directory() . '/languages');

function r_pagination($paged_navi = '', $pages = '')
{
    $range = 2;
    $showitems = 3;
    $passtest = false;

    global $paged;
    if (get_query_var('paged')) {
        $paged = get_query_var('paged');
    } else if (get_query_var('page')) {
        $paged = get_query_var('page');
    } else if ($paged_navi != '') {
        $paged = $paged_navi;
    } else {
        $paged = 1;
    }
    echo wp_kses_post($paged_navi);

    if ($paged_navi < 2)
        $range = 2;
    if ($paged_navi == 2)
        $range = 2;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (! $pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo '<ul class="navigate-page">';
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) {
            echo "<li><a href='" . get_pagenum_link(1) . "'>Previous</a></li> ";
        } else {
            echo "<li class='disabled'><a href='" . get_pagenum_link(1) . "'>Previous</a></li> ";
        }
        $j = 0;
        $linkpoints = "";
        for ($i = 1; $i <= $pages; $i ++) {

            if (1 != $pages && (! ($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {

                if ($j != 4) {
                    echo ($paged == $i) ? "<li class='active'><a href='#'>" . $i . "</a></li> " : "<li><a href='" . get_pagenum_link($i) . "' class='' >" . $i . "</a></li> ";
                    if ($paged == 1 || $paged == 2) {
                        $linkpoints = "<li><a href='" . get_pagenum_link($i + 1) . "' class='' >...</a></li> ";
                    }
                } else {
                    $linkpoints = "<li><a href='" . get_pagenum_link($i) . "' class='' >...</a></li> ";
                }
                $j ++;
            }
        }

        if ($paged < $pages) {
            if ($paged < ($pages - 2)) {
                if ($paged < ($pages - 3)) {
                    echo wp_kses_post($linkpoints);
                }
                echo "<li><a href='" . get_pagenum_link($pages - 1) . "'>" . ($pages - 1) . "</a></li> ";
            }
            echo "<li><a href='" . get_pagenum_link($pages) . "'>Next</a></li> ";
        } else {
            echo "<li><a href='" . get_pagenum_link($pages) . "'>Next</a></li> ";
        }
        echo '</ul>';
    }
    if ($passtest) {
        posts_nav_link();
    }
}

// Adding specific CSS class
add_filter('body_class', 'r_addinr_portfolio_class');

function r_addinr_portfolio_class($classes)
{
    if (r_option('style_page')) {
        $stylepage = is_page() && ! is_home();
    } else {
        $stylepage = false;
    }

    $classes[] = 'style-blog-' . r_option('style_blog');

    if (r_option('ajax_posts')) {
        $classes[] = 'loading-ajax-post';
    }
    if ($stylepage) {
        $classes[] = 'style-sidebar-right_s';
    } else {
        if (r_option('sidebar_s') == '') {
            $classes[] = 'style-sidebar-both_s';
        } else {
            $classes[] = 'style-sidebar-' . r_option('sidebar_s');
        }
    }
    $classes[] = get_post_format();

    if (r_option('header_white')) {
        $classes[] = 'header_white';
    }
    return $classes;
}

function r_register_menus()
{
    register_nav_menus(array(
        'main-menu' => 'Main Menu'
    ));
}
add_action('init', 'r_register_menus');

function r_word_limit()
{
    if (! is_singular()) {
        add_filter("the_content", "r_word_limit_op");
    }
}

function r_word_limit_op($content)
{
    $word_limit = r_option('word_limit');
    if (r_option('style_blog') == 'grid') {
        $word_limit = r_option('word_limit_grid');
    }
    if ($word_limit == "") {
        $word_limit = 40;
    }

    $words = explode(" ", $content);
    return implode(" ", array_splice($words, 0, $word_limit));
}

add_action('add_meta_boxes', 'r_meta_box_add_po');

function r_meta_box_add_po()
{
    $jobboard_slug = 'jobpost';
    if (r_option('jobboard_slug') != '') {
        $jobboard_slug = r_option('jobboard_slug');
    }
    add_meta_box('meta-box-one-page', __(' Details: ', 'onotes'), 'r_meta_box_po', $jobboard_slug, 'normal', 'high');
}

function r_meta_box_po($post)
{
    $values = get_post_custom($post->ID);
    $location = isset($values['location']) ? ($values['location'][0]) : '';

    wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce');
    ?>

<div id="portfolio_metabox" style="display: block important">
	<p>
	
	
	<h3>
		<label for="location"><?php _e('Location:','onotes') ?> </label>
	</h3>
	<input style="width: 100%" type="location" name="location"
		id="location" value="<?php echo esc_attr($location) ?>" />
	</p>
</div>
<?php
}

add_action('save_post', 'r_meta_box_save');

function r_meta_box_save($post_id)
{
    if (defined('DOINr_AUTOSAVE') && DOINr_AUTOSAVE)
        return;
    if (! isset($_POST['meta_box_nonce']) || ! wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
        return;
    if (! current_user_can('edit_page', $post_id))
        return;

    if (isset($_POST['location']))
        update_post_meta($post_id, 'location', $_POST['location']);
}

/* ----------------------------------------------------------------------------------- */
// Theme setup
/* ----------------------------------------------------------------------------------- */
add_action('after_setup_theme', 'r_theme_setup');

function r_theme_setup()
{
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-formats', array(
        'link',
        'gallery',
        'quote',
        'video',
        'audio'
    ));
    set_post_thumbnail_size(50, 50, true);
}

/* ----------------------------------------------------------------------------------- */
/*
 * Queue Scripts
 * /*-----------------------------------------------------------------------------------
 */
function r_scripts()
{
    $font_family_url1 = r_option('font_family_url1');
    if ($font_family_url1 != '') {
        wp_register_style('googlefont1', $font_family_url1);
    } else {
        wp_register_style('googlefont1', 'https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700');
    }
    //wp_enqueue_style('googlefont1');

    $font_family_url2 = r_option('font_family_url2');
    if ($font_family_url2 != '') {
        wp_register_style('googlefont2', $font_family_url2);
    } else {
        wp_register_style('googlefont2', 'https://fonts.googleapis.com/css?family=Rajdhani:400,300,600,500,700');
    }
    //wp_enqueue_style('googlefont2');

    if (r_option('style_page')) {
        $stylepage = is_page() && ! is_home();
    } else {
        $stylepage = false;
    }

    // ENQUEUE CSS
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    //wp_register_style('flaticon', get_template_directory_uri() . '/css/flaticon.css');
    wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_register_style('nprogress', get_template_directory_uri() . '/css/nprogress.css');
    //wp_register_style('mediaelementplayer', get_template_directory_uri() . '/css/mediaelementplayer.css');
    //wp_register_style('owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css');
    wp_register_style('style', get_template_directory_uri() . '/style.css');
    // if(r_option('sidebar_s')=='right_s'||($stylepage)){
    wp_register_style('responsive', get_template_directory_uri() . '/css/blog_style2.css');
    // }else{
    // wp_register_style( 'responsive', get_template_directory_uri() . '/css/responsive.css');
    // }

    wp_enqueue_style('bootstrap');
    //wp_enqueue_style('flaticon');
    //wp_enqueue_style('fontawesome');
    //wp_enqueue_style('nprogress');
    //wp_enqueue_style('mediaelementplayer');
    //wp_enqueue_style('owl.carousel');
    wp_enqueue_style('style');
    if (r_option('header_white')) {
        wp_register_style('header_white', get_template_directory_uri() . '/css/header-white.css');
        wp_enqueue_style('header_white');
    }
    wp_enqueue_style('responsive');

    // IS MAIN PAGE
    if(!is_page(2)) {
        if (r_option('custom_color') && r_option('color_pri')) {
            $path_color = get_template_directory_uri() . '/css/colors/' . r_option('color_pri') . '.css';
            wp_register_style('custom_color', $path_color);
            wp_enqueue_style('custom_color');
        } else {
            wp_register_style('custom_color', get_template_directory_uri() . '/css/colors/nephritis.css');
            wp_enqueue_style('custom_color');
        }
    }

    $logo_height = esc_attr(r_option('logo_height'));
    /*$font_family_name1 = esc_attr(r_option('font_family_name1'));
    $font_family_name2 = esc_attr(r_option('font_family_name2'));
    $custom_css = r_option('custom_css');
    if (r_option('font_family_name1') != 'Raleway') {
        $custom_css .= 'body { font-family: ' . r_option('font_family_name1') . ', sans-serif;}';
    }
    wp_add_inline_style('style', $custom_css);*/

    add_action('wp_head', function () {

        $header_font = r_option('font_family_name2');

        $style = (r_option('select-style') == 'style-2') ? '
            <style> body {background: #ffffff; }
                    .main-body>.row {margin-top: 0px !important;}
                    .featured-post-slider {display: none;}
                    .header .navbar .container-fluid>.row {border-bottom: 1px solid #f3f6f8;}
                    .right-sidebar {padding-top: 20px}
                    .left-sidebar .items {border-right: 1px solid #f3f6f8;}
            </style>' : '<style> body {background: #f4f5f9; } </style>';

        echo $style;

        if (isset($header_font)) {
            echo '<style> h1, h2, h3, h4, h5 { font-family:\'' . $header_font . '\', sans-serif;}
            .slider-area .carousel-item .item .content .content-head a{ font-family:\'' . $header_font . '\', sans-serif;}</style>';
        }
    });

    // ENQUEUE JAVSCRIPTS

    wp_enqueue_script('jquery');
    wp_enqueue_script('script2', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '', true);
    //wp_enqueue_script('script3', get_template_directory_uri() . '/js/mediaelement-and-player.min.js', array(), '', true);
    //wp_enqueue_script('script4', get_template_directory_uri() . '/js/tweetie.min.js', array(), '', true);
    //wp_enqueue_script('script5', get_template_directory_uri() . '/js/jquery.fitvids.js', array(), '', true);
    wp_enqueue_script('script6', get_template_directory_uri() . '/js/jquery.jscroll.js', array(), '', true);
    //wp_enqueue_script('script8', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), true);
    wp_enqueue_script('script7', get_template_directory_uri() . '/js/custom.js', array(), '', true);
    
    // INSERT JAVSCRIPT VARS
    wp_localize_script('jquery', 'templateUrl', get_template_directory_uri());
    wp_localize_script('script2', 'ajaxurl', admin_url('admin-ajax.php'));
    $styleblog = r_option('style_blog');

    wp_localize_script('script2', 'styleblogjs', $styleblog);
}
add_action('wp_enqueue_scripts', 'r_scripts');

function ieimport()
{
    echo '<!--[if lt IE 9]>';
    echo '<script src="' . get_template_directory_uri() . '/js/html5shiv.js"></script>';
    echo '<script src="' . get_template_directory_uri() . '/js/respond.min.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'ieimport');

function r_add_image_sizes()
{
    add_image_size('blog-post-thumbnail', 800, 405, true);
    add_image_size('portfolio-thumbnail', 390, 285, true);
    add_image_size('blog-thumbnail-vertical', 260, 350, true);
    add_image_size('reader_featured_post', 450, 335, true);
    add_image_size('popular_thumbnail', 310, 140, true);
}
//add_action('init', 'r_add_image_sizes');

function r_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    $width_co = 24;
    ?>
<li>
	<div class="row">
									<?php if(get_avatar( $comment, 128 )): ?>	
									<?php $width_co = 20; ?>									
                                    <div class="col-sm-4">
			<div class="thumb">
                                            <?php echo get_avatar( $comment, 128 ); ?>
                                        </div>
		</div>
									<?php endif; ?>
                                    <div
			class="col-sm-<?php echo esc_attr($width_co) ?>">
			<p><?php comment_text() ?></p>
			<p>
				<span class="author-comment"><?php comment_author_link()	?></span> <span><?php echo sprintf(__('%s ago','onotes'), human_time_diff(get_comment_date('U'))) ?> </span>
                                            <?php comment_reply_link(array_merge( $args, array('reply_text'=>__('Reply','onotes'),'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                                        </p>
		</div>
	</div>
	<hr class="small">
</li>



<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
<?php
}

/**
 * Sidebar widgets
 */
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Default sidebar',
        'id' => 'sidebar-1',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div><hr class="small">',
        'before_title' => '<h5 class="sr_w_title">',
        'after_title' => '</h5>'
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Left Sidebar',
        'id' => 'left-sidebar',
        'before_widget' => '<li class="widget %2$s" id="%1$s">',
        'after_widget' => '</li>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer 1',
        'id' => 'footer-1',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer 2',
        'id' => 'footer-2',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer 3',
        'id' => 'footer-3',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5>'
    ));
}

add_filter('get_avatar', 'r_gravatar_class');

function r_gravatar_class($class)
{
    $class = str_replace("class='avatar", "class='avatar thumb", $class);
    return $class;
}

function r_wp_title($title, $sep)
{
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    $title .= get_bloginfo('name', 'display');

    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        $title = "$title $sep $site_description";
    }

    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'onotes'), max($paged, $page));
    }

    return $title;
}
add_filter('wp_title', 'r_wp_title', 10, 2);

/**
 * TGM-Plugin-Activation
 */
require_once dirname(__FILE__) . '/include/class-tgm-plugin-activation.php';

function r_plugin_activation()
{
    $plugins = array(
        array(
            'name' => 'ShortcodesDex',
            'slug' => 'shortcodesdex',
            'source' => get_stylesheet_directory() . '/include/libs/shortcodesdex.zip',
            'required' => true, // If false, the plugin is only 'recommended' instead of required
            'version' => '1.2.6', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented. If the plugin version is higher than the plugin version installed , the user will be notified to update the plugin
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' => '' // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name' => 'Job Posting dex',
            'slug' => 'jobposting-dex',
            'source' => get_stylesheet_directory() . '/include/libs/jobposting-dex.zip',
            'required' => true, // If false, the plugin is only 'recommended' instead of required
            'version' => '1.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented. If the plugin version is higher than the plugin version installed , the user will be notified to update the plugin
            'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url' => '' // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name' => 'mailchimp',
            'slug' => 'mailchimp',
            'required' => false
        )
    );

    $theme_text_domain = 'onotes';

    $config = array(
        'domain' => $theme_text_domain, // Text domain - likely want to be the same as your theme.
        'default_path' => '', // Default absolute path to pre-packaged plugins
        'parent_menu_slug' => 'themes.php', // Default parent menu slug
        'parent_url_slug' => 'themes.php', // Default parent URL slug
        'menu' => 'install-required-plugins', // Menu slug
        'has_notices' => true, // Show admin notices or not
        'is_automatic' => false, // Automatically activate plugins after installation or not
        'message' => '', // Message to output right before the plugins table
        'strings' => array(
            'page_title' => __('Install Required Plugins', $theme_text_domain),
            'menu_title' => __('Install Plugins', $theme_text_domain),
            'installing' => __('Installing Plugin: %s', $theme_text_domain), // %1$s = plugin name
            'oops' => __('Something went wrong with the plugin API.', $theme_text_domain),
            'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.'), // %1$s = plugin name(s)
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.'), // %1$s = plugin name(s)
            'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.'), // %1$s = plugin name(s)
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.'), // %1$s = plugin name(s)
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.'), // %1$s = plugin name(s)
            'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.'), // %1$s = plugin name(s)
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.'), // %1$s = plugin name(s)
            'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.'), // %1$s = plugin name(s)
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins'),
            'activate_link' => _n_noop('Activate installed plugin', 'Activate installed plugins'),
            'return' => __('Return to Required Plugins Installer', $theme_text_domain),
            'plugin_activated' => __('Plugin activated successfully.', $theme_text_domain),
            'complete' => __('All plugins installed and activated successfully. %s', $theme_text_domain) // %1$s = dashboard link
        )
    );

    tgmpa($plugins, $config);
}
add_action('tgmpa_register', 'r_plugin_activation');

// Metaboxes
require get_template_directory() . '/include/CMB2-master/init.php';
require get_template_directory() . '/include/metaboxes.php';

function showlatestposts($posttype = 'post', $name = 'Snippets', $nos = 10) {
    $html = '';
    $query = new WP_Query( 'post_type='.$posttype.'&posts_per_page='.$nos );
    if ( $query->have_posts() ) : 
    $html = '<nav class="box-type-one"><h2>Recent '.get_bloginfo("name").' '.$name.'</h2>';
    $html .= '<ul>'; 
    while ( $query->have_posts() ) : $query->the_post();
    $html .= '<li><a href="'.get_the_permalink().'">'.get_the_title().'</a></li>'; 
    endwhile;
    $html .= '</ul>'; 
    $html .= '</nav>';
    else :
    endif;
    echo $html;
}

function fetchAllBlogs()
{
    global $wpdb;
    $blogs = $wpdb->get_results("SELECT * FROM $wpdb->blogs WHERE spam = '0' AND deleted = '0' and archived = '0' and public='1'");
    return $blogs;
}

function isLocalhost($whitelist = ['127.0.0.1', '::1'])
{
    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

function wp_recent_across_network($posttype = 'post', $name = 'Snippets', $return = false, $size = 5, $expires = 7200)
{
    if (! is_multisite())
        return false;

	//delete_transient( 'recent_across_network_'.$posttype );
    //delete_transient( 'multisite_site_list' );
	
	// Cache the results with the WordPress Transients API
    // Get any existing copy of our transient data
    $recent_across_network = get_site_transient('recent_across_network_'.$posttype);
    if ($recent_across_network === false || !is_array($recent_across_network)) {
    //if ($recent_across_network === false) {

        // No transient found, regenerate the data and save a new transient
        // Prepare the SQL query with $wpdb
        global $wpdb;
		$prefix = 'onenot_';
		
        // Because the get_blog_list() function is currently flagged as deprecated
        // due to the potential for high consumption of resources, we'll use
        // $wpdb to roll out our own SQL query instead. Because the query can be
        // memory-intensive, we'll store the results using the Transients API
        if (false === ($site_list = get_site_transient('multisite_site_list'))) {
            $site_list = $wpdb->get_results('SELECT * FROM ' . $prefix . 'blogs ORDER BY blog_id');
            set_site_transient('multisite_site_list', $site_list, $expires);
        }

        $limit = absint($size);

        // Merge the wp_posts results from all Multisite websites into a single result with MySQL "UNION"
        foreach ($site_list as $site) {
            if ($site == $site_list[0]) {
                $posts_table = $prefix . "posts";
            } else {
                $posts_table = $prefix . $site->blog_id . "_posts";
            }

            $posts_table = esc_sql($posts_table);
            $blogs_table = esc_sql($prefix . 'blogs');

            $query .= "(SELECT $posts_table.ID, $posts_table.post_title, $posts_table.post_date, $blogs_table.blog_id FROM $posts_table, $blogs_table";
            $query .= " WHERE $posts_table.post_type = '{$posttype}'";
            $query .= " AND $posts_table.post_status = 'publish'";
            $query .= " AND $blogs_table.blog_id = {$site->blog_id})";

            if ($site !== end($site_list))
                $query .= " UNION ";
            else
                $query .= " ORDER BY post_date DESC LIMIT 0, $limit";
        }

        // Sanitize and run the query
        $recent_across_network = $wpdb->get_results($query);

        // Set the Transients cache to expire every two hours
        set_site_transient('recent_across_network_'.$posttype, $recent_across_network, 60 * 60 * 2);
    }
    //echo $recent_across_network;
    // Format the HTML output
    if ($recent_across_network) {
        if ($return) {
            return $recent_across_network;
        } else {
            $html = '<nav class="box-type-one"><h2>Recent '.$name.'</h2>';
            $html .= '<ul>';
            foreach ($recent_across_network as $post) {
                $html .= '<li><a href="' . get_blog_permalink($post->blog_id, $post->ID) . '">' . $post->post_title . '</a></li>';
            }
            $html .= '</ul>';
            $html .= '</nav>';
            echo $html;
        }
    }
}
function ci_get_related_posts( $post_id, $related_count, $args = array() ) {
    $args = wp_parse_args( (array) $args, array(
        'orderby' => 'rand',
        'return'  => 'query', // Valid values are: 'query' (WP_Query object), 'array' (the arguments array)
    ) );
    
    $related_args = array(
        'post_type'      => get_post_type( $post_id ),
        'posts_per_page' => $related_count,
        'post_status'    => 'publish',
        'post__not_in'   => array( $post_id ),
        'orderby'        => $args['orderby'],
        'tax_query'      => array()
    );
    
    $post       = get_post( $post_id );
    $taxonomies = get_object_taxonomies( $post, 'names' );
    
    foreach ( $taxonomies as $taxonomy ) {
        $terms = get_the_terms( $post_id, $taxonomy );
        if ( empty( $terms ) ) {
            continue;
        }
        $term_list                   = wp_list_pluck( $terms, 'slug' );
        $related_args['tax_query'][] = array(
            'taxonomy' => $taxonomy,
            'field'    => 'slug',
            'terms'    => $term_list
        );
    }
    
    if ( count( $related_args['tax_query'] ) > 1 ) {
        $related_args['tax_query']['relation'] = 'OR';
    }
    
    if ( $args['return'] == 'query' ) {
        return new WP_Query( $related_args );
    } else {
        return $related_args;
    }
}
add_action( 'admin_menu', 'remove_admin_menus' );

//Remove top level admin menus
function remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'link-manager.php' );
    remove_menu_page( 'tools.php' );
    //remove_menu_page( 'plugins.php' );
    remove_menu_page( 'users.php' );
    //remove_menu_page( 'options-general.php' );
    remove_menu_page( 'upload.php' );
    //remove_menu_page( 'edit.php' );
    //remove_menu_page( 'edit.php?post_type=page' );
    //remove_menu_page( 'themes.php' );
}

function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Snippets';
    $submenu['edit.php'][5][0] = 'Snippets';
    $submenu['edit.php'][10][0] = 'Add Snippets';
    //$submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    //$submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    echo '';
}

function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Snippets';
    $labels->singular_name = 'Snippet';
    $labels->add_new = 'Add Snippet';
    $labels->add_new_item = 'Add Snippet';
    $labels->edit_item = 'Edit Snippets';
    $labels->new_item = 'Snippet';
    $labels->view_item = 'View Snippet';
    $labels->search_items = 'Search Snippets';
    $labels->not_found = 'No Snippets found';
    $labels->not_found_in_trash = 'No Snippets found in Trash';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );
function add_custom_post_type_to_query( $query ) {
    if ( is_home() && $query->is_main_query() ) {
        $query->set( 'post_type', array('post', 'question', 'source_code') );
    }
}
add_action( 'pre_get_posts', 'add_custom_post_type_to_query' );
function disable_embeds_tiny_mce_plugin($plugins) {
    return array_diff($plugins, array('wpembed'));
}
function disable_embeds_rewrites($rules) {
    foreach($rules as $rule => $rewrite) {
        if(false !== strpos($rewrite, 'embed=true')) {
            unset($rules[$rule]);
        }
    }
    return $rules;
}
function disable_embeds_code_init() {
    
    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );
    
    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );
    
    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
    
    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    
    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
    add_filter( 'tiny_mce_plugins', 'disable_embeds_tiny_mce_plugin' );
    
    // Remove all embeds rewrite rules.
    add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );
    
    // Remove filter of the oEmbed result before any HTTP requests are made.
    remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
	// REMOVE WP EMOJI
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');

	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	wp_deregister_style( 'wp-block-library' );
    if (!is_admin()) {
        wp_deregister_script('wp-embed');
        //wp_deregister_script('jquery');  // Bonus: remove jquery too if it's not required
    }
}
add_action( 'init', 'disable_embeds_code_init', 9999 );
function wps_deregister_styles() {
	wp_dequeue_style( 'print_emoji_styles' );
	//wp_dequeue_script( 'wp-embed' );
	// IS MAIN PAGE
	if(is_page(2)) {
	   wp_dequeue_script( 'crayon_js' );
	   wp_dequeue_script( 'script6' );
	   wp_dequeue_script( 'script7' );
	}
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function deq_wp_enqueue_scripts() {
	if(is_page(2)) {
		wp_dequeue_style( 'crayon' );
	}
}
add_action( 'wp_enqueue_scripts', 'deq_wp_enqueue_scripts',999 );
/*function deq_wp_footer(){

}
add_action( 'wp_footer', 'deq_wp_footer', 100 );
function deq_wp_head() {

}
add_action('wp_head', 'deq_wp_head');
function deq_wp_print_scripts() {

}
add_action( 'wp_print_scripts', 'deq_wp_print_scripts', 100 );
function deq_wp_print_styles() {
}
add_action( 'wp_print_styles', 'deq_wp_print_styles', 100 );*/
function prettyPrintMe($f){
    echo '<pre>';print_r($f);echo '</pre>';
}
function display_frontend_scripts() {
    global $enqueued_scripts;
    global $enqueued_styles;

    add_action('wp_print_scripts', 'cyb_list_scripts');

    function cyb_list_scripts()
    {
        global $wp_scripts;
        global $enqueued_scripts;
        $enqueued_scripts = array();
        foreach ($wp_scripts->queue as $handle) {
            $enqueued_scripts[] = $wp_scripts->registered[$handle]->src.'['.$handle.']';
        }
    }
    add_action('wp_print_styles', 'cyb_list_styles');

    function cyb_list_styles()
    {
        global $wp_styles;
        global $enqueued_styles;
        $enqueued_styles = array();
        foreach ($wp_styles->queue as $handle) {
            $enqueued_styles[] = $wp_styles->registered[$handle]->src.'['.$handle.']';
        }
    }

    add_action('wp_head', function () {
        global $enqueued_scripts;
        prettyPrintMe($enqueued_scripts);
        global $enqueued_styles;
        prettyPrintMe($enqueued_styles);
    });
}
//display_frontend_scripts();