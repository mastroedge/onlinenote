<?php 
$onepage = is_page_template('template-one-page.php');

$feedburner = esc_attr(r_option('feedburner'));

if(!empty($feedburner)){
	$feed_url = esc_url(r_option('feedburner'));
}else{
	$feed_url = esc_url(get_bloginfo('rss2_url'));
}
if(r_option('word_limit')){
	r_word_limit();
}
if(!$onepage){
$body_id = 'header-top';
}else{
$body_id = 'header-normal';
}
$logo_img = r_option('logo_img');
if ($logo_img =="") {
$logo_img = get_template_directory_uri().'/images/logo.png';
}
$logotext = r_option('logo_text');
if($logotext == ''){
//$logotext = get_bloginfo('name');
}
if(r_option('style_page')){
$stylepage = is_page()&&!is_home();
}else{
$stylepage = false;
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>

    <!-- TITLE OF SITE -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php /*<meta name="description" content="<?php bloginfo('description');?>"/> */ ?>
	<meta name="theme-color" content="#3367d6" />

	<?php if(r_option('responsive_mode')): ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<?php endif; ?>

    <!-- =========================
      FAV AND TOUCH ICONS  
    ============================== -->
	<?php
	$favicon = r_option('favicon');
	$favicon_iphone = r_option('favicon_iphone');
	$favicon_iphone_retina = r_option('favicon_iphone_retina');
	$favicon_ipad = r_option('favicon_ipad');
	$favicon_ipad_retina = r_option('favicon_ipad_retina');
	?>
	<?php if (!empty($favicon)) : ?>
	<link rel="shortcut icon" href="<?php echo esc_url($favicon); ?>" />
	<?php endif; ?>
	<?php if (!empty($favicon_iphone)) : ?>
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo esc_url($favicon_iphone); ?>" />
	<?php endif; ?>
	<?php if (!empty($favicon_iphone_retina)) : ?>
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url($favicon_iphone_retina); ?>" />
	<?php endif; ?>
	<?php if (!empty($favicon_ipad)) : ?>
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url($favicon_ipad); ?>" />
	<?php endif; ?>
	<?php if (!empty($favicon_ipad_retina)) : ?>
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url($favicon_ipad_retina); ?>" />
	<?php endif; ?>	

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name')?>" href="<?php echo esc_url($feed_url) ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name')?>" href="<?php bloginfo('atom_url');?>" />
    


<?php wp_head() ?>
</head>
<body id="<?php echo esc_attr($body_id) ?>" <?php body_class() ?>>
<header class="header">
    <nav class="navbar navbar-default" role="navigation">
                   <div class="container">
            <div class="row">
                    <div class="col-md-9 col-sm-24 head-left">
                    <a class="navbar-brand <?php if($logotext=='') {echo'logo-img';} ?>" href="<?php echo network_site_url(); ?>" title="Online Notes">
                        <!-- LOGO -->
                        <?php if(file_exists(get_template_directory() .'/images/onlinenote-logo.png')) { ?>
                        	<img src="<?php echo get_bloginfo('template_url').'/images/onlinenote-logo.png'; ?>" alt="Online Notes" class="logo">
                        <?php } else { ?>
                        	<img src="<?php echo esc_url($logo_img); ?>" alt="<?php bloginfo('name'); ?>" alt="Online Notes" class="logo">
                        <?php } ?>
					</a>
						<?php 
						$nameSite = explode('/', get_home_url());
						if(count($nameSite) > 3 && file_exists(get_template_directory() .'/images/onlinenote-'.end($nameSite).'.png')) {
						?>
						<div class="inner-logo">
						<a href="<?php echo get_home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo get_bloginfo('template_url').'/images/onlinenote-'.end($nameSite).'.png'; ?>" alt="<?php bloginfo('name'); ?>"></a>
                        </div>
						<?php } ?>
						<?php /*?><h1><?php echo esc_attr($logotext) ?></h1>*/?>
                    
                    </div>
                    <div class="col-md-4 col-sm-4"></div>
                    <!-- SEARCH -->
                    <div class="col-md-10 col-sm-24 pad-right">
                    <script async src="https://cse.google.com/cse.js?cx=015291088720924864323:omp9trjw4ia"></script>
						<div class="gcse-search"></div>
					<?php
					/*switch($_SERVER['REQUEST_URI']){
						case '/':
						case '/manu/onlinenotes/':
						?>
						<script>
						<script async src="https://cse.google.com/cse.js?cx=015291088720924864323:omp9trjw4ia"></script>
						<div class="gcse-search"></div>
						<?php
						break;
						default:
						?>
						<form class="search-box navbar-form" action="<?php echo home_url(); ?>" role="search">
                            <div class="input-group">
                                <input type="text" name="s" id="ss" class="form-control" placeholder="Click to Search">
                            </div><!-- /input-group -->
                        </form>
						<?php
						break;
					}*/
					?>
                    </div>
                    </div>
                    </div>
    </nav>
</header>