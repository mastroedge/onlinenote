<?php
/*
 * Template Name: With two sidebar
 *
 */
 get_header();
 //$container_s = (r_option('sidebar_left_page'))?'container-fluid':'container';
 //$blor_s = r_option('sidebar_left_page')?'col-md-14 col-sm-18':'col-md-18';
 $layout = (r_option('select-layout')=='container-fluid')?'container-fluid':'container';
 ?>
 
 <!-- BODY CONTAINER - FULL WIDTH -->

<div class="main-body clearfix <?php //echo esc_attr($layout); ?>">
		 <?php if(!isMobile()) { ?>
		<aside id="left-side-box" class="col-lg-5 col-md-5 col-sm-5 clearfix left-sidebar hidden-xs">
				<?php wp_recent_across_network(); ?>
		</aside>
		<?php } ?>
		<div id="main-home-box" class="col-lg-14 col-md-14 col-sm-18 clearfix">
		<div class="blog-style-one">
		<?php 
		while ( have_posts() ) : the_post();
		the_content();
		endwhile;
		?>
		</div>
		</div>
		<aside id="right-side-box" class="col-lg-5 col-md-5 col-sm-5 clearfix">
				<?php 
				if(isMobile()) {
				    wp_recent_across_network();
				}
				wp_recent_across_network('source_code', 'Examples');
				wp_recent_across_network('question', 'Questions'); ?>
		</aside>
</div> <!-- end of .container-fluid -->

<?php get_footer() ?>