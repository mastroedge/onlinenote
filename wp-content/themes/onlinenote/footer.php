<!-- =========================
     FOOTER SECTION 
============================== -->
<footer class="footer <?php //echo esc_attr(r_option('footer_style')) ?>">
	<?php /*if(!r_option('ajax_posts')||!is_home()): ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-14 col-md-offset-4">
                <div class="footer-widgets row">
                    <div class="col-footer col-sm-<?php echo esc_attr(r_option('width_foo1')) ?>">
						<?php dynamic_sidebar( 'footer-1' ); ?>
					</div>
                   <div class="col-footer col-sm-<?php echo esc_attr(r_option('width_foo2')) ?> ">
						<?php dynamic_sidebar( 'footer-2' ); ?>
					</div>
                    <div class="col-footer col-sm-<?php echo esc_attr(r_option('width_foo3')) ?> ">
						<?php dynamic_sidebar( 'footer-3' ); ?>
					</div>
                </div> <!-- end of .footer-widgets -->
            </div>
        </div>
    </div>
	<?php endif;*/ ?>
    <!-- /END CONTAINER -->

    <hr class="fullwid">
    <?php
    /*$nav_args = array(
        'theme_location' => 'main-menu',
        'depth'             => 1,
        'container'         => 'ul',
        'menu_id'      => 'footer-menu',
    );
    wp_nav_menu( $nav_args );*/
    ?>
    <div class="container-fluid">
		<ul>
		<li><a href="https://www.onlinenote.in/contact/">Contact</a><li>
		</ul>
        <p class="copyright"><?php echo wp_kses_post(r_option('copyrights')) ?></p>
        <?php /*if( is_multisite() ): ?>
   		The <?php echo esc_html( get_site_option( 'site_name' ) ); ?> network currently powers <?php echo get_blog_count(); ?> websites and <?php echo get_user_count(); ?> users.
		<?php endif;*/ ?>
    </div>

</footer>
<!-- /END FOOTER SECTION -->

<!-- ANALYTICS -->
<?php 
if(!isLocalhost()){
    echo stripslashes((r_option('analyticscode')));  
}
?>
<script type="text/javascript">
<?php echo stripslashes(esc_js(r_option('custom_js')));?>
</script>
<?php wp_footer(); ?>
</body>
</html>