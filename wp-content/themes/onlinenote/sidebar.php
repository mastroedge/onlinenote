        <?php if(isMobile()) { ?>
        <!-- =========================
             LEFT SIDEBAR - NAVIGATION 
        ============================== -->	
        <aside class="left-sidebar">
            <div class="col-md-5 col-sm-6 items <?php if(r_option('left_sidebar_fixed')) echo 'left-sidebar-fixed' ?>">
                <?php /*?> <nav class="box-type-one">
						<?php dynamic_sidebar( 'left-sidebar' ); ?>
                </nav> <!-- end of .left-navigation --> */ ?>
					<?php showlatestposts(); ?>
					<?php showlatestposts('source_code', 'Examples'); ?>
					<?php showlatestposts('question', 'Questions'); ?>
            </div>
        </aside>
        <!-- /END LEFT SIDEBAR - NAVIGATION -->
		<?php } ?>
        <!-- =========================
             RIGHT SIDEBAR 
        ============================== -->
        <aside class="right-sidebar">
        <div class="col-md-5 col-sm-6 items col-md-5">
				<?php //if(!function_exists('dynamic_sidebar')||!dynamic_sidebar()):?>
				<?php //endif;?>
    			<?php wp_recent_across_network(); ?>
				<?php wp_recent_across_network('source_code', 'Examples'); ?>
				<?php wp_recent_across_network('question', 'Questions'); ?>
        	</div>
        </aside>
        <!-- /END RIGHT SIDEBAR -->
	