<?php
/*
 * Template Name: Training Page
 *
 */
get_header();
$container_s = (r_option('sidebar_left_page'))?'container-fluid':'container';
$blor_s = r_option('sidebar_left_page')?'col-md-14 col-sm-18':'col-md-18';
$layout = (r_option('select-layout')=='container-fluid')?'container-fluid':'container';
?>
 
 <!-- BODY CONTAINER - FULL WIDTH -->

<div class="main-body-page <?php echo esc_attr($layout); ?>">
    <div class="row">
		<?php get_template_part('sidebar-page') ?>
 
        <div class="<?php echo esc_attr( $blor_s) ?>">
            <div class="blog-style-one">
                <!-- GENERAL BLOG POST -->
				
				<?php
						
						while ( have_posts() ) : the_post();
						$title_post = get_the_title();
						if($title_post==""){
							$title_post = '(Untitled)';
						}
						?>
                <article class="blog-item">
					
                    <header>
                        <h2 class="title">
                            <a href="<?php the_permalink() ?>"><?php echo esc_html($title_post) ?></a>
                        </h2>
                       
                    </header>
					<?php get_template_part( 'content', 'gallery' ); ?>
					<div class="post-body">
					<?php the_content() ?>
					<?php 
						wp_link_pages( array(
							'before'      => '<div class="pagination"><div class="navigate-page"><span class="page-links-title">' . __( 'Pages:', 'onotes' ) . '</span>',
							'after'       => '</div></div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
						) );
					?>
					</div>
                </article>
					
				<?php endwhile; ?>
                <!-- PAGINATION -->
                
                <!-- /PAGINATION -->
            </div>
        </div>
        <!-- /END BLOG SECTION -->
 <?php //get_sidebar() ?>
    </div>
</div> <!-- end of .container-fluid -->

<?php get_footer() ?>