<?php
/*
 * Template Name: Main Home
 *
 */
get_header();
//$container_s = (r_option('sidebar_s') == 'right_s') ? 'container' : 'container-fluid';
//$blor_s = (r_option('sidebar_s') == 'right_s') ? 'col-md-18' : 'col-md-14 col-sm-18';
$layout = (r_option('select-layout') == 'container-fluid') ? 'container-fluid' : 'container';
?>
<div class="main-body <?php //echo esc_attr($layout); ?>">
	<div class="row">
		<?php if(!isMobile()) { ?>
		<aside id="left-side-box" class="col-lg-5 col-md-5 col-sm-5 clearfix left-sidebar hidden-xs">
				<?php wp_recent_across_network(); ?>
		</aside>
		<?php } ?>
		<div id="main-home-box" class="col-lg-14 col-md-14 col-sm-18 clearfix">
		<?php 
		while ( have_posts() ) : the_post();
		the_content();
		endwhile;
		?>
			<?php /*<div class="row">
				<br>
        <?php
        $blogs = fetchAllBlogs();
        if (! empty($blogs)) {
            //$count_blog = count($blogs);
            foreach ($blogs as $blog) {
                //$key = array_search($blog, $blogs);
                $details = get_blog_details($blog->blog_id);
                if ($details != false) {
                    if (! (($blog->blog_id == 1) && ($show_main != 1))) {
                        ?>
                <div
					class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
					<a class="stag-icon-link" href="<?php echo $details->siteurl; ?>" title="<?php echo $details->blogname;?>"
						target="_self"> 
						<?php 
						$nameSite = explode('/', $details->siteurl);
						if(file_exists(get_template_directory() .'/images/onlinenote-'.end($nameSite).'.png')) { 
						    echo '<img alt="'.$details->blogname.'" src="'.get_bloginfo('template_url').'/images/onlinenote-'.end($nameSite).'.png">';
						 } else { ?>
						<svg aria-hidden="true"
							class="svg-inline--fa fa-cubes fa-w-16"
							style="font-size: 100px; line-height: 100px;" data-prefix="fas"
							data-icon="cubes" role="img" xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 512 512" data-fa-i2svg="">
               <path fill="currentColor"
								d="M488.6 250.2L392 214V105.5c0-15-9.3-28.4-23.4-33.7l-100-37.5c-8.1-3.1-17.1-3.1-25.3 0l-100 37.5c-14.1 5.3-23.4 18.7-23.4 33.7V214l-96.6 36.2C9.3 255.5 0 268.9 0 283.9V394c0 13.6 7.7 26.1 19.9 32.2l100 50c10.1 5.1 22.1 5.1 32.2 0l103.9-52 103.9 52c10.1 5.1 22.1 5.1 32.2 0l100-50c12.2-6.1 19.9-18.6 19.9-32.2V283.9c0-15-9.3-28.4-23.4-33.7zM358 214.8l-85 31.9v-68.2l85-37v73.3zM154 104.1l102-38.2 102 38.2v.6l-102 41.4-102-41.4v-.6zm84 291.1l-85 42.5v-79.1l85-38.8v75.4zm0-112l-102 41.4-102-41.4v-.6l102-38.2 102 38.2v.6zm240 112l-85 42.5v-79.1l85-38.8v75.4zm0-112l-102 41.4-102-41.4v-.6l102-38.2 102 38.2v.6z"></path>
            </svg> <!-- <i aria-hidden="true" class="fas fa-cubes" style="font-size:100px;line-height:100px"></i> -->
            <br><?php echo $details->blogname;?>
            <?php } ?>
					</a>
         
      </div>
                <?php
                    }
                }
            }
        }
        ?>
       <div class="clear"></div>
			</div>*/ ?>
		</div>
		<aside id="right-side-box" class="col-lg-5 col-md-5 col-sm-5 clearfix">
				<?php 
				if(isMobile()) {
				    wp_recent_across_network();
				}
				wp_recent_across_network('source_code', 'Examples');
				wp_recent_across_network('question', 'Questions'); ?>
		</aside>

	</div>
</div>
<?php
get_footer();