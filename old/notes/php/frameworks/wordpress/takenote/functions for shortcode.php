<?php
//Activate post-image functionality (WP 2.9+)
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );
add_image_size( 'sidebar-thumb', 120, 120, true ); // Hard Crop Mode
add_image_size( 'homepage-thumb', 220, 180 ); // Soft Crop Mode
add_image_size( 'singlepost-thumb', 590, 9999 ); // Unlimited Height Mode
add_image_size( 'small_thumb', 84, 101 ); // Soft Crop Mode
add_image_size( 'about_thumb', 138, 140 ); // Soft Crop Mode
add_image_size( 'news_thumb', 250, 160 ); 

//Featured image
function about_thumb(){
if ( has_post_thumbnail() ) {
the_post_thumbnail( 'about_thumb', array('class' => 'about_thumb') );
} else {
}
}
//Featured image
function custom_image_label(){
if ( has_post_thumbnail() ) {
the_post_thumbnail( 'custom-image', array('class' => 'custom-image') );
} else {
}
}
//Featured image
function small_thumb(){
if ( has_post_thumbnail() ) {
the_post_thumbnail( 'small_thumb', array('class' => 'small_thumb') );
} else {
}
}
//Featured image
function news_thumb(){
if ( has_post_thumbnail() ) {
the_post_thumbnail( 'news_thumb', array('class' => 'news_thumb') );
} else {
}
}

// Display Images from Gallery of a post and page
function gallery_display1(){
$iPostID = $post->ID;
$arrImages =& get_children('post_type=attachment&numberposts=50&post_mime_type=image&post_parent=' . $iPostID );
if($arrImages) {
$arrKeys = array_keys($arrImages);

foreach( $arrKeys as $value) {
$thumbUrl = wp_get_attachment_thumb_url($value);
$image_attributes = wp_get_attachment_image_src( $value, "large" );	 // or ( $value, array(478, 286));
$bigUrl = $image_attributes[0];
$bigWidth = $image_attributes[1];					// not if the values set
?>
<li><a href="<?php echo $bigUrl; ?>"><img src="<?php echo $thumbUrl; ?>" alt="" /></a></li>
<?php
}
}
}

// Display Images from Gallery of a post and page for Slider
function gallery_display2(){
query_posts('category_name=logos');		// Change Category here
while (have_posts()) : the_post(); 
$argsThumb = array(
'order' => 'DESC',
'post_type' => 'attachment',
'post_parent' => $post->ID,
'post_mime_type' => 'image',
'numberposts' => -1,
'post_status' => null
);
$attachments = get_posts($argsThumb);
if ($attachments) {
foreach ($attachments as $attachment)
{
?>
<img src="<?php echo wp_get_attachment_url($attachment->ID, $size = 'full', false, false); ?>" />
<?php
}
} 
endwhile; 
}

// Display Post from category
function displaycat_post($catname, $nopost){
query_posts('category_name='.$catname.'&showposts='.$nopost.'&orderby=date&order=desc');
if (have_posts()) : while (have_posts()) : the_post();
the_title();
the_content();
endwhile; endif;
wp_reset_query();
}

// Shotcodes
function quote( $atts, $content = null ) {
	//return '<div class="right text">"'.$content.'"</div>';
	$array = array (
	'<p>[' => '[',
	']</p>' => ']',
	']<br />' => ']'
	);
	$content = strtr($content, $array);
	return '<div class="quote text">"'.do_shortcode($content).'"</div>';	// Nested Shotcode
}
add_shortcode("quote", "quote");
function heading3( $atts, $content = null ) {
	return '<h3>'.$content.'</h3>';
}
add_shortcode("h3", "heading3");

// Shotcodes Buttons
add_action('init', 'add_button');		// Hook into WordPress
function add_button() {					// Create Our Initialization Function
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_plugin');
     add_filter('mce_buttons', 'register_button');
   }
}
function register_button($buttons) {	// Register Our Button
   array_push($buttons, "quote,heading3,Lorem");
   return $buttons;
}
function add_plugin($plugin_array) {	// Register Our TinyMCE Plugin
   $plugin_array['quote'] = get_bloginfo('template_url').'/js/shotcodebutton.js';
   $plugin_array['heading3'] = get_bloginfo('template_url').'/js/shotcodebutton.js';
   $plugin_array['Lorem'] = get_bloginfo('template_url').'/js/shotcodebutton.js';
   return $plugin_array;
}

// Get URL of first image in a post
function catch_that_image() {
global $post, $posts;
$first_img = '';
ob_start();
ob_end_clean();
$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
$first_img = $matches [1] [0];

// no image found display default image instead
if(empty($first_img)){
$first_img = "/images/no-image.jpg";
}
return $first_img;
}

// Enable Custom Background
add_custom_background();

//Include Comments Template
include('functions/comments.php');

// Short Content
function short_content($num) {
$limit = $num+1;
$FormatedContent= get_the_content();
$content= preg_replace('/<img[^>]+./',"",$FormatedContent);
$content = str_split($content); 
$length = count($content);
if ($length>=$num) {
$content = array_slice( $content, 0, $num);
$content = implode("",$content)."...";
echo '<p>'.$content.'</p>';
}
else {
the_content();
}
}
function short_expln($content,$num) {
$limit = $num+1;
$content = strip_tags($content);
$content = str_split($content);
$length = count($content);
if ($length>=$num) {
$content = array_slice( $content, 0, $num);
$content = implode("",$content)."...";
echo '<p>'.$content.'</p>';
}
else {
echo '<p>'.$content.'</p>';
}
}

//Register Sidebars
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Sidebar',
'description' => 'Widgets in this area will be shown on the right-hand side.',
'before_widget' => '<div class="box">',
'after_widget' => '</div>',
'before_title' => '<h4>',
'after_title' => '</h4>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Home Top Right',
'description' => 'Widgets in this area will be shown on homepage business loop on the top right side next to the main description.',
'before_widget' => '<div class="home-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Home Bottom Left',
'description' => 'Widgets in this area will be shown on homepage business loop on the left side.',
'before_widget' => '<div class="home-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Home Bottom Middle',
'description' => 'Widgets in this area will be shown on homepage business loop in the middle.',
'before_widget' => '<div class="home-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Home Bottom Right',
'description' => 'Widgets in this area will be shown on homepage business loop on the right side.',
'before_widget' => '<div class="home-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Contact Form',
'description' => 'Contact Form',
'before_widget' => '<div class="home-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>',
));

// Limit Post Word Count
function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

//Replace Excerpt Link
function gpp_excerpt($text) { 
	return str_replace('[...]', '...', $text); 
	} 
add_filter('the_excerpt', 'gpp_excerpt');

// Get Post Categories
$categories = get_categories('hide_empty=0&orderby=name');
$wp_cats = array();
foreach ($categories as $category_list ) {
       $wp_cats[$category_list->cat_ID] = $category_list->cat_name;
}
array_unshift($wp_cats, "Choose a category");

// Register Navigation Menus
register_nav_menus(
	array(
	'primary'=>__('Top Menu'),
	'footerlinks'=>__('Footer Menu'),
	'leftlinks'=>__('Footer Left Menu'),
	'middilelinks'=>__('Footer Middile Menu'),
	'rightlinks'=>__('Footer Right Menu'),
	'innerlinks'=>__('Page Top Menu'),
	'sitemap_page'=>__('Sitemap Page Menu'),
	'sitemap_post'=>__('Sitemap Post Menu'),
	)
);

// Start Admin Panel
$themename = "Theme Panel";
$shortname = "xs";

// Theme Options
$options = array (

array( "name" => $themename." Options",
	"type" => "title"),

array( "name" => "Main Options - Important",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Custom Favicon",
	"desc" => "A favicon is a 16x16 pixel icon that represents your site; paste the URL to a .ico image that you want to use as the image",
	"id" => $shortname."_favicon",
	"type" => "text",
	"std" => get_bloginfo('url') ."/favicon.ico"),
	
array( "name" => "Custom Logo",
	"desc" => "Insert the URL to your logo here",
	"id" => $shortname."_custom_logo",
	"type" => "text",
	"std" => get_bloginfo('template_url') ."/images/logo.png"),
	
array( "name" => "Enable Logo Tag Line",
	"desc" => "Check this box if you would like to Enable the title next to the logo",
	"id" => $shortname."_disable_logo_text",
	"type" => "checkbox",
	"std" => "false"),

array( "name" => "Tag Line",
	"desc" => "Enter Tag Line For Your Business.",
	"id" => $shortname."_tag_line",
	"type" => "text",
	"std" => ""),

array( "name" => "Homepage Layout",
	"desc" => "Choose between business or blog style homepage layout.",
	"id" => $shortname."_home_layout",
	"type" => "select",
	"options" => array("business", "blog","personal"),
	"std" => "business"),
	
array( "name" => "Blog Category",
	"desc" => "Choose a category for the posts on your blog page",
	"id" => $shortname."_blog_cat",
	"type" => "select",
	"options" => $wp_cats,
	"std" => "Choose a category"),
	
array( "name" => "Portfolio Category",
	"desc" => "Choose a category for your portfolio page",
	"id" => $shortname."_portfolio_cat",
	"type" => "select",
	"options" => $wp_cats,
	"std" => "Choose a category"),

array( "name" => "Portfolio Item Count",
	"desc" => "How many items do you want to show on each portfolio page?",
	"id" => $shortname."_portfolio_count",
	"type" => "text",
	"std" => "12"),
	
array( "type" => "close"),
array( "name" => "Contact/Social Info",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Disable Phone Number",
	"desc" => "Check this box if you would like to Disable the phone number in the theme header",
	"id" => $shortname."_disable_phone",
	"type" => "checkbox",
	"std" => "false"),
	
array( "name" => "Phone Number",
	"desc" => "Enter A Phone Number For Your Business.",
	"id" => $shortname."_phone_number",
	"type" => "text",
	"std" => "9696009600"),
	
array( "name" => "Disable Email Address",
	"desc" => "Check this box if you would like to Disable the Email Address in the theme header",
	"id" => $shortname."_disable_email",
	"type" => "checkbox",
	"std" => "false"),
	
array( "name" => "Email Address",
	"desc" => "Enter A Email Address For Your Business.",
	"id" => $shortname."_email_address",
	"type" => "text",
	"std" => "info@yourdomain.com"),
	
array( "name" => "Twitter",
	"desc" => "Enter your twitter id.",
	"id" => $shortname."_twitter",
	"type" => "text",
	"std" => "#"),
	
array( "name" => "Facebook",
	"desc" => "Enter your Facebook Fan Page.",
	"id" => $shortname."_facebook",
	"type" => "text",
	"std" => "#"),
	
array( "name" => "LinkedIN",
	"desc" => "Enter your Linked IN Fan Page.",
	"id" => $shortname."_linkedin",
	"type" => "text",
	"std" => "#"),
	
array( "type" => "close"),
array( "name" => "Business Homepage Style Options",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Main Title",
	"desc" => "Enter a title for the main section on the business-style homepage",
	"id" => $shortname."_business_title",
	"type" => "text",
	"std" => "Editable region via the theme panel"),
	
array( "name" => "Main Text",
	"desc" => "Enter the text for the main section on the business-style homepage",
	"id" => $shortname."_business_text",
	"type" => "textarea",
	"std" => "Editable region via the theme panel"),

array( "type" => "close"),
array( "name" => "Homepage Featured Slider",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Disable Homepage Slider",
	"desc" => "Check this box if you would like to Disable the homepage slider. Turned off by default.",
	"id" => $shortname."_disable_slider",
	"type" => "checkbox",
	"std" => "false"),

array( "name" => "Featured Category",
	"desc" => "Choose a category from which featured posts are drawn",
	"id" => $shortname."_feat_cat",
	"type" => "select",
	"options" => $wp_cats,
	"std" => "Choose a category"),
	
array( "name" => "How Many Slides",
	"desc" => "Enter how many slides you wish to show on the featured slider.",
	"id" => $shortname."_feat_cat_slides",
	"type" => "text",
	"std" => "3"),

array( "name" => "Pause Time",
	"desc" => "Choose the amount of time in milliseconds between each slide",
	"id" => $shortname."_slider_pause",
	"type" => "text",
	"std" => "3000"),

array( "name" => "Slider Speed",
	"desc" => "Choose the transition speed in milliseconds for your slider",
	"id" => $shortname."_slider_speed",
	"type" => "text",
	"std" => "500"),

array( "name" => "Slider Slices",
	"desc" => "Choose number of slices for the slider animation",
	"id" => $shortname."_slider_slices",
	"type" => "text",
	"std" => "15"),
	
array( "name" => "Animation",
	"desc" => "Choose an animation for your slider",
	"id" => $shortname."_slider_animation",
	"type" => "select",
	"options" => array("fade", "fold", "random", "sliceDown"),
	"std" => "fade"),
	
array( "name" => "Directional Navigation",
	"desc" => "Choose true to enable the directional navigation. Turned on by default",
	"id" => $shortname."_slider_nav",
	"options" => array("true", "false",),
	"std" => "true"),
	
array( "name" => "Manually Add Items To Your Slider (advanced)",
	"desc" => "Here you can manually add mroe items to your slider. Add them in the same way you would add an image to a website using the HTML img src= tag",
	"id" => $shortname."_manual_slides",
	"type" => "textarea",
	"std" => ""),
	
array( "type" => "close"),
array( "name" => "Thumbnail Images",
	"type" => "section"),
array( "type" => "open"),
	
array( "name" => "Disable Thumbnails On Archieve/Category/Blog pages",
	"desc" => "Check this box if you would like to disable the thumbnail images on archieve/category pages.",
	"id" => $shortname."_disable_thumbnails_archieve",
	"type" => "checkbox",
	"std" => "false"),
	
array( "name" => "Disable Thumbnails On Blog Posts",
	"desc" => "Check this box if you would like to disable the thumbnail images on your blog posts.",
	"id" => $shortname."_disable_thumbnails_posts",
	"type" => "checkbox",
	"std" => "false"),
	
array( "name" => "Thumbnail Width",
	"desc" => "Type a width in pixels for post pages, blog page & category Page.",
	"id" => $shortname."_thumbnail_height",
	"type" => "text",
	"std" => "120"),
	
array( "name" => "Thumbnail Height",
	"desc" => "Type a width in pixels for post pages, blog page & category Page.",
	"id" => $shortname."_thumbnail_width",
	"type" => "text",
	"std" => "120"),

array( "type" => "close"),

array( "name" => "Color Scheme",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Choose A Color Scheme",
	"desc" => "Choose a color scheme for your site - Green is default",
	"id" => $shortname."_color_scheme",
	"type" => "select",
	"options" => array("blue","red","green"),
	"std" => "blue"),
	
array( "type" => "close"),

array( "name" => "Analytics",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Google Analytics Code",
	"desc" => "You can paste your Google Analytics or other tracking code in this box. This will be automatically added to the footer.",
	"id" => $shortname."_ga_code",
	"type" => "textarea",
	"std" => ""),
	
array( "type" => "close")

);

// Define Admin Panel
function mytheme_add_admin() {
global $themename, $shortname, $options;
if ( $_GET['page'] == basename(__FILE__) ) {
	if ( 'save' == $_REQUEST['action'] ) {
		foreach ($options as $value) {
		update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
foreach ($options as $value) {
	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }
	header("Location: admin.php?page=functions.php&saved=true");
die;
}
else if( 'reset' == $_REQUEST['action'] ) {
	foreach ($options as $value) {
		delete_option( $value['id'] ); }
	header("Location: admin.php?page=functions.php&reset=true");
die;
}
}
add_menu_page($themename, $themename, 'administrator', basename(__FILE__), 'mytheme_admin');
}

function mytheme_add_init() {
$file_dir=get_bloginfo('template_directory');
wp_enqueue_style("functions", $file_dir."/functions/xspanel.css", false, "1.0", "all");
wp_enqueue_script("rm_script", $file_dir."/js/rm_script.js", false, "1.0");
}

function mytheme_admin() {
global $themename, $shortname, $options;
$i=0;
if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
?>
<div class="wrap rm_wrap">
<h2><?php echo $themename; ?> Settings</h2>
<div class="rm_opts">
<form method="post">
<?php foreach ($options as $value) {
switch ( $value['type'] ) {
case "open":
?>
<?php break;
case "close":
?>
</div>
</div>
<br />

<?php break;

case "title":
?>
<h3>Theme Options</h3>
<?php break;
case 'text':
?>

<div class="rm_input rm_text">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
 	<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'])  ); } else { echo $value['std']; } ?>" />
 <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
 </div>
<?php
break;

case 'textarea':
?>

<div class="rm_input rm_textarea">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
 	<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo $value['std']; } ?></textarea>
 <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>

 </div>

<?php
break;

case 'select':
?>

<div class="rm_input rm_select">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>

<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
<?php foreach ($value['options'] as $option) { ?>
		<option <?php if (get_settings( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
</select>

	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
</div>
<?php
break;

case "checkbox":
?>

<div class="rm_input rm_checkbox">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>

<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />

	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
 </div>
<?php break;
case "section":
$i++;
?>

<div class="rm_section">
<div class="rm_title"><h3><img src="<?php bloginfo('template_directory')?>/functions/images/trans.png" class="inactive" alt="""><?php echo $value['name']; ?></h3><span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save changes" />
</span><div class="clearfix"></div></div>
<div class="rm_options">

<?php break;
}
}
?>

<input type="hidden" name="action" value="save" />
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>
<div>
</div>
<?php } ?>
<?php add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->term_id}.php") ) return TEMPLATEPATH . "/single-{$cat->term_id}.php"; } return $t;' )); ?>
<?php
add_action('admin_init', 'mytheme_add_init');
add_action('admin_menu', 'mytheme_add_admin');
?>
<?php
//WordPress Widget – Exclude some categories
class limited_catagories_list_widget extends WP_Widget {
        function limited_catagories_list_widget(){
                $widget_ops = array( 'classname' => 'Selective categories', 'description' => 'Show a list of Categories, with the ability to exclude categories' );
                $control_ops = array( 'id_base' => 'some-cats-widget' );
                $this->WP_Widget( 'some-cats-widget', 'Selective Catagories', $widget_ops, $control_ops );
        }
 
        function form ( $instance){
                $defaults = array( 'title' => 'Catagories', 'cats' => '' );
                $instance = wp_parse_args( (array) $instance, $defaults );
                ?>
                <p>
                        <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
                        <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
                </p>
                <p>
                        <label for="<?php echo $this->get_field_id( 'cats' ); ?>">Categories to exclude(comma separated list of Category IDs): </label>
                        <input id="<?php echo $this->get_field_id( 'cats' ); ?>" name="<?php echo $this->get_field_name( 'cats' ); ?>" value="<?php echo $instance['cats']; ?>" style="width:100%;" />
                </p>
                <?php
        }
 
        function update($new_instance, $old_instance) {
                $instance = $old_instance;
                $instance['title'] = strip_tags( $new_instance['title'] );
                $instance['cats'] = strip_tags( $new_instance['cats'] );
                return $instance;
        }
 
        function widget($args, $instance){
                extract( $args );
                $title = apply_filters('widget_title', $instance['title'] );
                $cats = $instance['cats'];
                echo $before_widget;
                if ( $title )
                        echo $before_title . $title . $after_title;
                echo "<ul>";
                wp_list_categories("exclude=$cats&title_li=");
                echo "</ul>";
                echo $after_widget;
        }
 
}
 
function register_jorbin_widget(){
                register_widget('limited_catagories_list_widget');
}
 
add_action('widgets_init', 'register_jorbin_widget');
 

// show Menu Description 
class My_Walker extends Walker_Nav_Menu
{
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
?>