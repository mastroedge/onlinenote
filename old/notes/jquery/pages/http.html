<p>The Hypertext Transfer Protocol (HTTP) is an application-level protocol for distributed, collaborative, hypermedia information systems. This is the foundation for data communication for the World Wide Web (i.e. internet) since 1990. HTTP is a generic and stateless protocol which can be used for other purposes as well using extensions of its request methods, error codes, and headers.</p>
<h3>Uniform Resource Identifiers</h3>
<p>Uniform Resource Identifiers (URI) are simply formatted, case-insensitive string containing name, location, etc. to identify a resource, for example, a website, a web service, etc. A general syntax of URI used for HTTP is as follows:</p>
<pre>URI = "http:" "//" host [ ":" port ] [ abs_path [ "?" query ]]</pre>
<h3>HTTP - Messages</h3>
<p>HTTP is based on the client-server architecture model and a stateless request/response protocol that operates by exchanging messages across a reliable TCP/IP connection.</p>
<pre>HTTP-message   = <Request> | <Response> ; HTTP/1.1 messages</pre>
<pre>
<ul class="list">
<li>A Start-line</li>
<li>Zero or more header fields followed by CRLF</li>
<li>An empty line (i.e., a line with nothing preceding the CRLF) 
indicating the end of the header fields</li>
<li>Optionally a message-body</li>
</ul>
</pre>
<h4>- Message Start-Line</h4>
<pre>
GET /hello.htm HTTP/1.1     (This is Request-Line sent by the client)

HTTP/1.1 200 OK             (This is Status-Line sent by the server)
</pre>
<h4>- Header Fields</h4>
<ul>
<li><p><b>General-header:</b> These header fields have general applicability for both request and response messages.</p></li>
<li><p><b>Request-header:</b> These header fields have applicability only for request messages.</p></li>
<li><p><b>Response-header:</b> These header fields have applicability only for response messages.</p></li>
<li><p><b>Entity-header:</b> These header fields define meta information about the entity-body or, if no body is present, about the resource identified by the request.</p></li>
</ul>
<p>Following are the examples of various header fields:</p>
<pre>
User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
Host: www.example.com
Accept-Language: en, mi
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache
Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Accept-Ranges: bytes
Content-Length: 51
Vary: Accept-Encoding
Content-Type: text/plain
</pre>

<h4>- Message Body</h4>
<pre>
&lt;html&gt;
   &lt;body&gt;
   
      &lt;h1&gt;Hello, World!&lt;/h1&gt;
   
   &lt;/body&gt;
&lt;/html&gt;
</pre>

<h3>HTTP - Methods</h3>
<table class="table table-bordered">
<tbody><tr>
<th>S.N.</th>
<th>Method and Description</th>
</tr>
<tr>
<td>1</td>
<td><b>GET</b><p>The GET method is used to retrieve  information from the given server using a given URI. Requests using GET should only retrieve data and should have no other effect on the data.</p></td>
</tr>
<tr>
<td>2</td>
<td><b>HEAD</b><p>Same as GET, but transfers the status line and header section only.</p></td>
</tr>
<tr>
<td>3</td>
<td><b>POST</b><p>A POST request is used to send data to the server, for example, customer information, file upload, etc. using HTML forms.</p></td>
</tr>
<tr>
<td>4</td>
<td><b>PUT</b><p>Replaces all current representations of the target resource with the uploaded content.</p></td>
</tr>
<tr>
<td>5</td>
<td><b>DELETE</b><p>Removes all current representations of the target resource given by a URI.</p></td>
</tr>
<tr>
<td>6</td>
<td><b>CONNECT</b><p>Establishes a tunnel to the server identified by a given URI.</p></td>
</tr>
<tr>
<td>7</td>
<td><b>OPTIONS</b><p>Describes the communication options for the target resource.</p></td>
</tr>
<tr>
<td>8</td>
<td><b>TRACE</b><p>Performs a message loop-back test along the path to the target resource.</p></td>
</tr>
</tbody></table>

<h2>GET Method</h2>
<p>A GET request retrieves data from a web server by specifying parameters in the URL portion of the request. This is the main method used for document retrieval. The following example makes use of GET method to fetch hello.htm:</p>
<pre class="result notranslate">GET /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
</pre>
<p>The server response against the above GET request will be as follows:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
</pre>
<pre style="" class="prettyprint notranslate prettyprinted"><span class="tag">&lt;html&gt;</span><span class="pln">
</span><span class="tag">&lt;body&gt;</span><span class="pln">
</span><span class="tag">&lt;h1&gt;</span><span class="pln">Hello, World!</span><span class="tag">&lt;/h1&gt;</span><span class="pln">
</span><span class="tag">&lt;/body&gt;</span><span class="pln">
</span><span class="tag">&lt;/html&gt;</span></pre>
<h2>HEAD Method</h2>
<p>The HEAD method is functionally similar to GET, except that the server replies with a response line and headers, but no entity-body. The following example makes use of HEAD method to fetch header information about hello.htm:</p>
<pre class="result notranslate">HEAD /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
</pre>
<p>The server response against the above GET request will be as follows:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
</pre>
<p>You can notice that here server the does not send any data after header.</p>
<h2>POST Method</h2>
<p>The POST method is used when you want to send some data to the server, for example, file update, form data, etc. The following example makes use of POST method to send  a form data to the server, which will be processed by a process.cgi and finally a response will be returned:</p>
<pre class="result notranslate">POST /cgi-bin/process.cgi HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Content-Type: text/xml; charset=utf-8
Content-Length: 88
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
</pre><pre style="" class="prettyprint notranslate prettyprinted"><span class="pun">&lt;?</span><span class="pln">xml version</span><span class="pun">=</span><span class="str">"1.0"</span><span class="pln"> encoding</span><span class="pun">=</span><span class="str">"utf-8"</span><span class="pun">?&gt;</span><span class="pln">
</span><span class="tag">&lt;string</span><span class="pln"> </span><span class="atn">xmlns</span><span class="pun">=</span><span class="atv">"http://clearforest.com/"</span><span class="tag">&gt;</span><span class="pln">string</span><span class="tag">&lt;/string&gt;</span></pre>
<p>The server side script process.cgi processes the passed data and sends the following response:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
</pre><pre style="" class="prettyprint notranslate prettyprinted"><span class="tag">&lt;html&gt;</span><span class="pln">
</span><span class="tag">&lt;body&gt;</span><span class="pln">
</span><span class="tag">&lt;h1&gt;</span><span class="pln">Request Processed Successfully</span><span class="tag">&lt;/h1&gt;</span><span class="pln">
</span><span class="tag">&lt;/body&gt;</span><span class="pln">
</span><span class="tag">&lt;/html&gt;</span></pre>
<h2>PUT Method</h2>
<p>The PUT method is used to request the server to store the included entity-body at a location specified by the given URL. The following example requests the server to save the given entity-boy in <b>hello.htm</b> at the root of the server:</p>
<pre class="result notranslate">PUT /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Accept-Language: en-us
Connection: Keep-Alive
Content-type: text/html
Content-Length: 182
</pre><pre style="" class="prettyprint notranslate prettyprinted"><span class="tag">&lt;html&gt;</span><span class="pln">
</span><span class="tag">&lt;body&gt;</span><span class="pln">
</span><span class="tag">&lt;h1&gt;</span><span class="pln">Hello, World!</span><span class="tag">&lt;/h1&gt;</span><span class="pln">
</span><span class="tag">&lt;/body&gt;</span><span class="pln">
</span><span class="tag">&lt;/html&gt;</span></pre>
<p>The server will store the given entity-body in <b>hello.htm</b> file and will send the following response back to the client:</p>
<pre class="result notranslate">HTTP/1.1 201 Created
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Content-type: text/html
Content-length: 30
Connection: Closed
</pre><pre style="" class="prettyprint notranslate prettyprinted"><span class="tag">&lt;html&gt;</span><span class="pln">
</span><span class="tag">&lt;body&gt;</span><span class="pln">
</span><span class="tag">&lt;h1&gt;</span><span class="pln">The file was created.</span><span class="tag">&lt;/h1&gt;</span><span class="pln">
</span><span class="tag">&lt;/body&gt;</span><span class="pln">
</span><span class="tag">&lt;/html&gt;</span></pre>
<h2>DELETE Method</h2>
<p>The DELETE method is used to request the server to delete a file at a location specified by the given URL. The following example requests the server to delete the given file <b>hello.htm</b> at the root of the server:</p>
<pre class="result notranslate">DELETE /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.tutorialspoint.com
Accept-Language: en-us
Connection: Keep-Alive
</pre>
<p>The server will delete the mentioned file <b>hello.htm</b> and will send the following response back to the client:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Content-type: text/html
Content-length: 30
Connection: Closed
</pre><pre style="" class="prettyprint notranslate prettyprinted"><span class="tag">&lt;html&gt;</span><span class="pln">
</span><span class="tag">&lt;body&gt;</span><span class="pln">
</span><span class="tag">&lt;h1&gt;</span><span class="pln">URL deleted.</span><span class="tag">&lt;/h1&gt;</span><span class="pln">
</span><span class="tag">&lt;/body&gt;</span><span class="pln">
</span><span class="tag">&lt;/html&gt;</span></pre>
<h2>CONNECT Method</h2>
<p>The CONNECT method is used by the client to establish a network connection to a web server over HTTP. The following example requests a connection with a web server running on the host tutorialspoint.com:</p>
<pre class="result notranslate">CONNECT www.tutorialspoint.com HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
</pre>
<p>The connection is established with the server and the following response is sent back to the client:</p>
<pre class="result notranslate">HTTP/1.1 200 Connection established
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
</pre>
<h2>OPTIONS Method</h2>
<p>The OPTIONS method is used by the client to find out the HTTP methods and other options supported by a web server. The client can specify  a URL for the OPTIONS method,  or an asterisk (*) to refer to the entire server. The following example requests a list of methods supported by a web server running on tutorialspoint.com:</p>
<pre class="result notranslate">OPTIONS * HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
</pre>
<p>The server will send an information based on the current configuration of the server, for example:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Allow: GET,HEAD,POST,OPTIONS,TRACE
Content-Type: httpd/unix-directory
</pre>
<h2>TRACE Method</h2>
<p>The TRACE method is used to echo the contents of an HTTP Request back to the requester which can be used for debugging purpose at the time of development. The following example shows the usage of TRACE method:</p>
<pre class="result notranslate">TRACE / HTTP/1.1
Host: www.tutorialspoint.com
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
</pre>
<p>The server will send the following message in response to the above request:</p>
<pre class="result notranslate">HTTP/1.1 200 OK
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache/2.2.14 (Win32)
Connection: close
Content-Type: message/http
Content-Length: 39

TRACE / HTTP/1.1
Host: www.tutorialspoint.com
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
</pre>

<h3>HTTP - Status Codes</h3>
<p>The Status-Code element in a server response, is a 3-digit integer where the first digit of the Status-Code defines the class of response and the last two digits do not have any categorization role. There are 5 values for the first digit:</p>
<table class="table table-bordered">
<tbody><tr>
<th>S.N.</th>
<th>Code and Description</th>
</tr>
<tr>
<td>1</td>
<td><b>1xx: Informational</b><p> It means the request has been received and the process is continuing.</p></td>
</tr>
<tr>
<td>2</td>
<td><b>2xx: Success</b><p> It means  the action was successfully received, understood, and accepted.</p></td>
</tr>
<tr>
<td>3</td>
<td><b>3xx: Redirection</b><p> It means further action must be taken in order to complete the request.</p></td>
</tr>
<tr>
<td>4</td>
<td><b>4xx: Client Error</b><p> It means the request contains incorrect syntax or cannot be fulfilled.</p></td>
</tr>
<tr>
<td>5</td>
<td><b>5xx: Server Error</b><p> It means the server failed to fulfill an apparently valid request.</p></td>
</tr>
</tbody></table>
<p>HTTP status codes are extensible and HTTP applications are not required to understand the meaning of all the registered status codes. Given below is a list of all the status codes.</p>
<h2>1xx: Information</h2>
<table class="table table-bordered">
  <tbody><tr>
    <th>Message</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>100 Continue</td>
    <td> Only a part of the request has been received by the server, but as long as it has not been rejected, the client should continue with the request.</td>
  </tr>
  <tr>
    <td>101 Switching Protocols</td>
    <td>The server switches protocol.</td>
  </tr>
</tbody></table>
<h2>2xx: Successful</h2>
<table class="table table-bordered">
  <tbody><tr>
    <th>Message</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>200 OK</td>
    <td>The request is OK.</td>
  </tr>
  <tr>
    <td>201 Created</td>
    <td>The request is complete, and a new resource is created&nbsp;.</td>
  </tr>
  <tr>
    <td>202 Accepted</td>
    <td>The request is accepted for processing, but the processing is not complete.
    </td>
  </tr>
  <tr>
    <td>203 Non-authoritative Information</td>
    <td>The information in the entity header is from a local or third-party copy, not from the original server.</td>
  </tr>
  <tr>
    <td>204 No Content</td>
    <td>A status code and a header are given in the response, but there is no entity-body in the reply.</td>
  </tr>
  <tr>
    <td>205 Reset Content</td>
    <td>The browser should clear the form used for this transaction for additional input.</td>
  </tr>
  <tr>
    <td>206 Partial Content</td>
    <td>The server is returning partial data of the size requested. Used in response to a request specifying a <i>Range</i> header. The server must specify the range included in the response with the <i>Content-Range</i> header.</td>
  </tr>
</tbody></table>
<h2>3xx: Redirection</h2>
<table class="table table-bordered">
  <tbody><tr>
    <th>Message</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>300 Multiple Choices</td>
    <td>A link list. The user can select a link and go to that location. Maximum five addresses &nbsp;.</td>
  </tr>
  <tr>
    <td>301 Moved Permanently</td>
    <td>The requested page has moved to a new url&nbsp;.
    </td>
  </tr>
  <tr>
    <td>302 Found</td>
    <td>The requested page has moved temporarily to a new url&nbsp;.</td>
  </tr>
  <tr>
    <td>303 See Other</td>
    <td>The requested page can be found under a different url&nbsp;.</td>
  </tr>
  <tr>
    <td>304 Not Modified</td>
    <td>This is the response code to an <i>If-Modified-Since</i> or <i>If-None-Match</i> header, where the URL has not been modified since the specified date.</td>
  </tr>
  <tr>
    <td>305 Use Proxy</td>
    <td>The requested URL must be accessed through the proxy mentioned in the <i>Location</i> header.</td>
  </tr>
  <tr>
    <td>306 <i>Unused</i></td>
    <td>This code was used in a previous version. It is no longer used, but the code is reserved.</td>
  </tr>
  <tr>
    <td>307 Temporary Redirect</td>
    <td>The requested page has moved temporarily to a new url.
    </td>
  </tr>
</tbody></table>
<h2>4xx: Client Error</h2>
<table class="table table-bordered">
  <tbody><tr>
    <th>Message</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>400&nbsp;Bad Request</td>
    <td>The server did not understand the request.</td>
  </tr>
  <tr>
    <td>401 Unauthorized</td>
    <td>The requested page needs a username and a password.</td>
  </tr>
  <tr>
    <td>402 Payment Required</td>
    <td><i>You can not use this code yet</i>.</td>
  </tr>
  <tr>
    <td>403 Forbidden</td>
    <td>Access is forbidden to the requested page.</td>
  </tr>
  <tr>
    <td>404 Not Found</td>
    <td>The server can not find the requested page.</td>
  </tr>
  <tr>
    <td>405 Method Not Allowed</td>
    <td>The method specified in the request is not allowed.</td>
  </tr>
  <tr>
    <td>406 Not Acceptable</td>
    <td>The server can only generate a response that is not accepted by the client.</td>
  </tr>
  <tr>
    <td>407 Proxy Authentication Required</td>

    <td>You must authenticate with a proxy server before this request can be served.</td>
  </tr>
  <tr>
    <td>408 Request Timeout</td>
    <td>The request took longer than the server was prepared to wait.</td>
  </tr>
  <tr>
    <td>409 Conflict</td>
    <td>The request could not be completed because of a conflict.</td>
  </tr>
  <tr>
    <td>410 Gone</td>
    <td>The requested page is no longer available&nbsp;.</td>
  </tr>
  <tr>
    <td>411 Length Required</td>
    <td>The "Content-Length" is not defined. The server will not accept the request without it&nbsp;.</td>
  </tr>
  <tr>
    <td>412 Precondition Failed</td>
    <td>The pre condition given in the request evaluated to false by the server.</td>
  </tr>
  <tr>
    <td>413 Request Entity Too Large</td>
    <td>The server will not accept the request, because the request entity is too large.</td>
  </tr>
  <tr>
    <td>414 Request-url Too Long</td>
    <td>The server will not accept the request, because the url is too long. Occurs when you convert a "post" request to a "get" request with a long query information&nbsp;.</td>
  </tr>
  <tr>
    <td>415 Unsupported Media Type</td>
    <td>The server will not accept the request, because the mediatype is not supported&nbsp;.</td>
  </tr>
  <tr>
    <td>416 Requested Range Not Satisfiable</td>
    <td>The requested byte range is not available and is out of bounds.</td>
  </tr>
  <tr>
    <td>417 Expectation Failed</td>
    <td>The expectation given in an Expect request-header field could not be met by this server.</td>
  </tr>
</tbody></table>
<h2>5xx: Server Error</h2>
<table class="table table-bordered">
  <tbody><tr>
    <th>Message</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>500&nbsp;Internal Server Error</td>
    <td>The request was not completed. The server met an unexpected condition.</td>
  </tr>
  <tr>
    <td>501 Not Implemented</td>
    <td>The request was not completed. The server did not support the functionality required.</td>
  </tr>
  <tr>
    <td>502 Bad Gateway</td>
    <td>The request was not completed. The server received an invalid response from the upstream server.</td>
  </tr>
  <tr>
    <td>503 Service Unavailable</td>
    <td>The request was not completed. The server is temporarily overloading or down.</td>
  </tr>
  <tr>
    <td>504 Gateway Timeout</td>
    <td>The gateway has timed out.</td>
  </tr>
  <tr>
    <td>505 HTTP Version Not Supported</td>
    <td>The server does not support the "http protocol" version.</td>
  </tr>
</tbody></table>
