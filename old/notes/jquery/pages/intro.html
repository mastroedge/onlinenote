<p>Traditionally, the onload handler for the window instance is used for this purpose,
  executing statements after the entire page is fully loaded. The syntax is typically something
  like</p>
<pre>
    window.onload = function() {
    	// do stuff here
    };
</pre>
<p>This causes the defined code to execute after the document has fully loaded. Unfortunately, the browser not only delays executing the onload code until after the DOM tree is created, but also waits until after all external resources are fully loaded and the page is displayed in the browser window. This includes not only resources like images, but QuickTime and Flash videos embedded in web pages, and there are more and more of them these days. As a result, visitors can experience a serious delay between the time that they first see the page and the time that the onload script is executed.</p>
<p>A much better approach would be to wait only until the document structure is fully parsed and the browser has converted the HTML into its resulting DOM tree before executing the script to apply the rich behaviors. Accomplishing this in a cross-browser manner is somewhat difficult, but jQuery provides a simple means to trigger the execution of code once the DOM tree has loaded (without waiting for external resources). The formal syntax to define such code (using our hiding example) is as follows:</p>
<pre>
    jQuery(document).ready(function() {
    	$("div.notLongForThisWorld").hide();
    });
</pre>
<p>First, we wrap the document instance with the jQuery() function, and then we apply the ready() method, passing a function to be executed when the document is ready to be manipulated.</p>
<p>We called that the formal syntax for a reason; a shorthand form, used much more frequently, is as follows:</p>
<pre>
    jQuery(function() {
    	$("div.notLongForThisWorld").hide();
    });
</pre>
<h3>Selecting Elements</h3>
<p>To collect a group of elements, we pass the selector to the jQuery function using the simple syntax</p>
<pre>
    $(selector)
    or
    jQuery(selector)
</pre>
<p>The $() function (an alias for the jQuery() function) returns a special JavaScript object containing an array of the DOM elements, in the order in which they are defined within the document, that match the selector. This object possesses a large number of useful predefined methods that can act on the collected group of elements.</p>
<table width="100%" border="1" cellspacing="0" cellpadding="3">
  <tr>
    <th scope="col">Selector type</th>
    <th scope="col">CSS</th>
    <th scope="col">jQuery</th>
    <th scope="col">What it does</th>
  </tr>
  <tr>
    <td>Tag name</td>
    <td>p { }</td>
    <td>$('p')</td>
    <td>This selects all paragraphs in the document.</td>
  </tr>
  <tr>
    <td>ID</td>
    <td>#some-id { }</td>
    <td>$('#some-id')</td>
    <td>This selects the single element in the document that has an ID of some-id.</td>
  </tr>
  <tr>
    <td>Class</td>
    <td>.some-class { }</td>
    <td>$('.some-class')</td>
    <td>This selects all elements in the document that have a class of some-class.</td>
  </tr>
</table>
<p>jQuery supports not only the selectors that you have already come to know and love, but also more advanced selectors—defined as part of the CSS specification — and even some custom selectors.</p>
<table width="100%" cellspacing="2" cellpadding="4" border="1">
  <tr>
    <th scope="col">Selector</th>
    <th scope="col">Results</th>
  </tr>
  <tr>
    <td>$(&quot;p:even&quot;)</td>
    <td>Selects all even &lt;p&gt; elements</td>
  </tr>
  <tr>
    <td>$(&quot;tr:nth-child(1)&quot;)</td>
    <td>Selects the first row of each table</td>
  </tr>
  <tr>
    <td>$(&quot;body &gt; div&quot;)</td>
    <td>Selects direct &lt;div&gt; children of &lt;body&gt;</td>
  </tr>
  <tr>
    <td>$(&quot;a[href$= 'pdf ']&quot;)</td>
    <td>Selects links to PDF files</td>
  </tr>
  <tr>
    <td>$(&quot;body &gt; div:has(a)&quot;)</td>
    <td>Selects direct &lt;div&gt; children of &lt;body&gt;-containing links</td>
  </tr>
  <tr>
    <td>$("*")</td>
    <td>Selects all elements</td>
  </tr>
  <tr>
    <td>$(this)</td>
    <td>Selects the current HTML element</td>
  </tr>
  <tr>
    <td>$("p.intro")</td>
    <td>Selects all &lt;p&gt; elements with class="intro"</td>
  </tr>
  <tr>
    <td>$("p:first")</td>
    <td>Selects the first &lt;p&gt; element</td>
  </tr>
  <tr>
    <td>$("ul li:first")</td>
    <td>Selects the first &lt;li&gt; element of the first &lt;ul&gt;</td>
  </tr>
  <tr>
    <td>$("ul li:first-child")</td>
    <td>Selects the first &lt;li&gt; element of every &lt;ul&gt;</td>
  </tr>
  <tr>
    <td>$("[href]")</td>
    <td>Selects all elements with an href attribute</td>
  </tr>
  <tr>
    <td>$("a[target='_blank']")</td>
    <td>Selects all &lt;a&gt; elements with a target attribute value equal to "_blank"</td>
  </tr>
  <tr>
    <td>$("a[target!='_blank']")</td>
    <td>Selects all &lt;a&gt; elements with a target attribute value NOT equal to "_blank"</td>
  </tr>
  <tr>
    <td>$(":button")</td>
    <td>Selects all &lt;button&gt; elements and &lt;input&gt; elements of type="button"</td>
  </tr>
  <tr>
    <td>$("tr:even")</td>
    <td>Selects all even &lt;tr&gt; elements</td>
  </tr>
  <tr>
    <td>$("tr:odd")</td>
    <td>Selects all odd &lt;tr&gt; elements</td>
  </tr>
</table>
<h3>The basic CSS selectors supported by jQuery</h3>
<table width="100%" cellspacing="2" cellpadding="4" border="1">
  <tr>
    <th scope="col">Selector</th>
    <th scope="col">Description</th>
  </tr>
  <tr>
    <td>*</td>
    <td>Matches any element.</td>
  </tr>
  <tr>
    <td>E</td>
    <td>Matches all elements with tag name E.</td>
  </tr>
  <tr>
    <td>E F</td>
    <td>Matches all elements with tag name F that are descendants of E.<br />
      E&gt;F Matches all elements with tag name F that are direct children of E.</td>
  </tr>
  <tr>
    <td>E+F</td>
    <td>Matches all elements with tag name F that are immediately preceded by sibling E.</td>
  </tr>
  <tr>
    <td>E~F</td>
    <td>Matches all elements with tag name F preceded by any sibling E.</td>
  </tr>
  <tr>
    <td>E.C</td>
    <td>Matches all elements with tag name E with class name C. Omitting E is the same as *.C.</td>
  </tr>
  <tr>
    <td>E#I</td>
    <td>Matches all elements with tag name E with the id of I. Omitting E is the same as *#I.</td>
  </tr>
  <tr>
    <td>E[A]</td>
    <td>Matches all elements with tag name E that have attribute A of any value.</td>
  </tr>
  <tr>
    <td>E[A=V]</td>
    <td>Matches all elements with tag name E that have attribute A whose value is exactly V.</td>
  </tr>
  <tr>
    <td>E[A^=V]</td>
    <td>Matches all elements with tag name E that have attribute A whose value starts with V.</td>
  </tr>
  <tr>
    <td>E[A$=V]</td>
    <td>Matches all elements with tag name E that have attribute A whose value ends with V.</td>
  </tr>
  <tr>
    <td>E[A!=V]</td>
    <td>Matches all elements with tag name E that have attribute A whose value doesn't match<br />
      the value V, or that lack attribute A completely.</td>
  </tr>
  <tr>
    <td>E[A*=V]</td>
    <td>Matches all elements with tag name E that have attribute A whose value contains V.</td>
  </tr>
</table>
<h3>jQuery Event Methods</h3>
<table border="1" width="100%;">
  <tbody>
    <tr>
      <th style="width:23%">Mouse Events</th>
      <th style="width:25%">Keyboard Events</th>
      <th style="width:22%">Form Events</th>
      <th>Document/Window Events</th>
    </tr>
    <tr>
      <td>click</td>
      <td>keypress</td>
      <td>submit</td>
      <td>load</td>
    </tr>
    <tr>
      <td>dblclick</td>
      <td>keydown</td>
      <td>change</td>
      <td>resize</td>
    </tr>
    <tr>
      <td>mouseenter</td>
      <td>keyup</td>
      <td>focus</td>
      <td>scroll</td>
    </tr>
    <tr>
      <td>mouseleave</td>
      <td>&nbsp;</td>
      <td>blur</td>
      <td>unload</td>
    </tr>
  </tbody>
</table>

<h3>jQuery Event Methods</h3>
<table border="1" width="100%;">
  <tbody>
    <tr>
      <th style="width:30%">Event</th>
      <th style="width:70%">Occurs When...</th>
    </tr>
    <tr>
      <td>click</td>
      <td>keypress</td>
    </tr>
  </tbody>
</table>