<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'manu_onlinenotes');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'du*q3OnHba-T]LJ)a|<TGPLP NlL0cNO@:x57D~R<FL!:q8kA(KWDTHzv4aDb>3B');
define('SECURE_AUTH_KEY',  'x`mj>ET7vj9oiYc;I,wb,MQb|bY=1T$%4I8$Ip;<jJ@:|yRC6_v8qua(NjJEDII)');
define('LOGGED_IN_KEY',    'PvSkrRBW7SAg5!K2UfXoE{4m5I~TBn-.6/^&id0jBzU(bYZc.Sg5!q%qtsB&A>JE');
define('NONCE_KEY',        'kwUUseFxM9{=@FVH/cd?VA,BsDPJ}K5`}w: TNwa3ftul,NRM ^&[&?PVgV)sni_');
define('AUTH_SALT',        '>`q`:6hN&y@>o!Sv8d9T+rYqB{b`_*+QP 9;VQJ+#AWd@6wxk<lQ_F7-#HTzd:^n');
define('SECURE_AUTH_SALT', 'P0=]Qn%87nM{^LRx*QYIH~4o,bz1ko*9Fp6QFd] GgA&r4^zvaMY?W3mWhyIy!q5');
define('LOGGED_IN_SALT',   '58%0y6jKC&{SM%#@mZ!vWUh:> W_7C[Jbx1V,]Pqd7yx2UnzM# ^aj&E!+ 2J=ac');
define('NONCE_SALT',       'Hk:B?GRsgpIg1xE)KwbTb:[hM^Q7z@78P;#=sBUZ7KfWp,>K2r L@SH&A;,sDY25');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'onenot_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('AUTOSAVE_INTERVAL', 300 ); // seconds
define('WP_POST_REVISIONS', 2 );

/* That's all, stop editing! Happy blogging. */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/manu/onlinenotes/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

//define ( 'WP_CONTENT_FOLDERNAME', 'wp-content' );
//define ( 'WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME );
//define ( 'WP_SITEURL', 'http://localhost/manu/onlinenotes/' );
//define ( 'WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME );
	
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
